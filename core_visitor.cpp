#include "core_visitor.hpp"

#include <cassert>

ASTVisitor::~ASTVisitor() {}

void ASTVisitor::visit(I32ConstExprAST *node)
{
    assert(false && "Visitor accessed undefined AST node.");
}

void ASTVisitor::visit(F64ConstExprAST *node)
{
    assert(false && "Visitor accessed undefined AST node.");
}

void ASTVisitor::visit(BoolConstExprAST *node)
{
    assert(false && "Visitor accessed undefined AST node.");
}

void ASTVisitor::visit(VarExprAST *node)
{
    assert(false && "Visitor accessed undefined AST node.");
}

void ASTVisitor::visit(AddExprAST *node)
{
    assert(false && "Visitor accessed undefined AST node.");
}

void ASTVisitor::visit(SubExprAST *node)
{
    assert(false && "Visitor accessed undefined AST node.");
}

void ASTVisitor::visit(MulExprAST *node)
{
    assert(false && "Visitor accessed undefined AST node.");
}

void ASTVisitor::visit(DivExprAST *node)
{
    assert(false && "Visitor accessed undefined AST node.");
}

void ASTVisitor::visit(ModExprAST *node)
{
    assert(false && "Visitor accessed undefined AST node.");
}

void ASTVisitor::visit(EqExprAST *node)
{
    assert(false && "Visitor accessed undefined AST node.");
}

void ASTVisitor::visit(NEqExprAST *node)
{
    assert(false && "Visitor accessed undefined AST node.");
}

void ASTVisitor::visit(LTExprAST *node)
{
    assert(false && "Visitor accessed undefined AST node.");
}

void ASTVisitor::visit(GTExprAST *node)
{
    assert(false && "Visitor accessed undefined AST node.");
}

void ASTVisitor::visit(LEqExprAST *node)
{
    assert(false && "Visitor accessed undefined AST node.");
}

void ASTVisitor::visit(GEqExprAST *node)
{
    assert(false && "Visitor accessed undefined AST node.");
}

void ASTVisitor::visit(AndExprAST *node)
{
    assert(false && "Visitor accessed undefined AST node.");
}

void ASTVisitor::visit(OrExprAST *node)
{
    assert(false && "Visitor accessed undefined AST node.");
}

void ASTVisitor::visit(NegExprAST *node)
{
    assert(false && "Visitor accessed undefined AST node.");
}

void ASTVisitor::visit(NotExprAST *node)
{
    assert(false && "Visitor accessed undefined AST node.");
}

void ASTVisitor::visit(AssignExprAST *node)
{
    assert(false && "Visitor accessed undefined AST node.");
}

void ASTVisitor::visit(CallExprAST *node)
{
    assert(false && "Visitor accessed undefined AST node.");
}

void ASTVisitor::visit(ExpressionStmtAST *node)
{
    assert(false && "Visitor accessed undefined AST node.");
}

void ASTVisitor::visit(DeclarationStmtAST *node)
{
    assert(false && "Visitor accessed undefined AST node.");
}

void ASTVisitor::visit(PrintStmtAST *node)
{
    assert(false && "Visitor accessed undefined AST node.");
}

void ASTVisitor::visit(ReturnStmtAST *node)
{
    assert(false && "Visitor accessed undefined AST node.");
}

void ASTVisitor::visit(CompoundStmtAST *node)
{
    assert(false && "Visitor accessed undefined AST node.");
}

void ASTVisitor::visit(IfThenStmtAST *node)
{
    assert(false && "Visitor accessed undefined AST node.");
}

void ASTVisitor::visit(IfThenElseStmtAST *node)
{
    assert(false && "Visitor accessed undefined AST node.");
}

void ASTVisitor::visit(WhileStmtAST *node)
{
    assert(false && "Visitor accessed undefined AST node.");
}

void ASTVisitor::visit(ImportAST *node)
{
    assert(false && "Visitor accessed undefined AST node.");
}

void ASTVisitor::visit(FunctionAST *node)
{
    assert(false && "Visitor accessed undefined AST node.");
}

void ASTVisitor::visit(ModuleAST *node)
{
    assert(false && "Visitor accessed undefined AST node.");
}
