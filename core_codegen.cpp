#include "core_codegen.hpp"

#include "core_typecheck.hpp"

CodeGenVisitor::CodeGenVisitor(ModuleManager &module_manager)
    : m_module_manager(module_manager),
      builder(context)
{
}

void CodeGenVisitor::visit(I32ConstExprAST *node)
{
    m_current_value =
        llvm::ConstantInt::get(context, llvm::APInt(32, node->m_value));
}

void CodeGenVisitor::visit(F64ConstExprAST *node)
{
    m_current_value =
        llvm::ConstantFP::get(context, llvm::APFloat(node->m_value));
}

void CodeGenVisitor::visit(BoolConstExprAST *node)
{
    m_current_value = llvm::ConstantInt::getBool(context, node->m_value);
}

void CodeGenVisitor::visit(VarExprAST *node)
{
    llvm::Type *llvm_type = core_to_llvm(node->m_entry->type, context);
    m_current_value = builder.CreateLoad(llvm_type, node->m_entry->address,
                                         node->m_name.to_string());
}

void CodeGenVisitor::visit(AddExprAST *node)
{
    node->m_left->accept(*this);
    auto left_value = m_current_value;
    node->m_right->accept(*this);
    auto right_value = m_current_value;

    TypeCheckVisitor type_checker;
    node->accept(type_checker);
    Type expr_type = type_checker.get_deduced_type();
    if (expr_type == Type::INT32)
        m_current_value = builder.CreateAdd(left_value, right_value, "addtmp");
    else if (expr_type == Type::FLOAT64)
        m_current_value = builder.CreateFAdd(left_value, right_value, "addtmp");
}

void CodeGenVisitor::visit(SubExprAST *node)
{
    node->m_left->accept(*this);
    auto left_value = m_current_value;
    node->m_right->accept(*this);
    auto right_value = m_current_value;

    TypeCheckVisitor type_checker;
    node->accept(type_checker);
    Type expr_type = type_checker.get_deduced_type();
    if (expr_type == Type::INT32)
        m_current_value = builder.CreateSub(left_value, right_value, "subtmp");
    else if (expr_type == Type::FLOAT64)
        m_current_value = builder.CreateFSub(left_value, right_value, "subtmp");
}

void CodeGenVisitor::visit(MulExprAST *node)
{
    node->m_left->accept(*this);
    auto left_value = m_current_value;
    node->m_right->accept(*this);
    auto right_value = m_current_value;

    TypeCheckVisitor type_checker;
    node->accept(type_checker);
    Type expr_type = type_checker.get_deduced_type();
    if (expr_type == Type::INT32)
        m_current_value = builder.CreateMul(left_value, right_value, "multmp");
    else if (expr_type == Type::FLOAT64)
        m_current_value = builder.CreateFMul(left_value, right_value, "multmp");
}

void CodeGenVisitor::visit(DivExprAST *node)
{
    node->m_left->accept(*this);
    auto left_value = m_current_value;
    node->m_right->accept(*this);
    auto right_value = m_current_value;

    TypeCheckVisitor type_checker;
    node->accept(type_checker);
    Type expr_type = type_checker.get_deduced_type();
    if (expr_type == Type::INT32)
        m_current_value = builder.CreateSDiv(left_value, right_value, "divtmp");
    else if (expr_type == Type::FLOAT64)
        m_current_value = builder.CreateFDiv(left_value, right_value, "divtmp");
}

void CodeGenVisitor::visit(ModExprAST *node)
{
    node->m_left->accept(*this);
    auto left_value = m_current_value;
    node->m_right->accept(*this);
    auto right_value = m_current_value;
    m_current_value = builder.CreateSRem(left_value, right_value, "modtmp");
}

void CodeGenVisitor::visit(EqExprAST *node)
{
    node->m_left->accept(*this);
    auto left_value = m_current_value;
    node->m_right->accept(*this);
    auto right_value = m_current_value;

    TypeCheckVisitor type_checker;
    node->m_left->accept(type_checker);
    Type expr_type = type_checker.get_deduced_type();
    if (expr_type == Type::INT32)
        m_current_value =
            builder.CreateICmpEQ(left_value, right_value, "eqtmp");
    else if (expr_type == Type::FLOAT64)
        m_current_value =
            builder.CreateFCmpUEQ(left_value, right_value, "eqtmp");
}

void CodeGenVisitor::visit(NEqExprAST *node)
{
    node->m_left->accept(*this);
    auto left_value = m_current_value;
    node->m_right->accept(*this);
    auto right_value = m_current_value;

    TypeCheckVisitor type_checker;
    node->m_left->accept(type_checker);
    Type expr_type = type_checker.get_deduced_type();
    if (expr_type == Type::INT32)
        m_current_value =
            builder.CreateICmpNE(left_value, right_value, "neqtmp");
    else if (expr_type == Type::FLOAT64)
        m_current_value =
            builder.CreateFCmpUNE(left_value, right_value, "neqtmp");
}

void CodeGenVisitor::visit(LTExprAST *node)
{
    node->m_left->accept(*this);
    auto left_value = m_current_value;
    node->m_right->accept(*this);
    auto right_value = m_current_value;

    TypeCheckVisitor type_checker;
    node->m_left->accept(type_checker);
    Type expr_type = type_checker.get_deduced_type();
    if (expr_type == Type::INT32)
        m_current_value =
            builder.CreateICmpSLT(left_value, right_value, "lttmp");
    else if (expr_type == Type::FLOAT64)
        m_current_value =
            builder.CreateFCmpULT(left_value, right_value, "lttmp");
}

void CodeGenVisitor::visit(GTExprAST *node)
{
    node->m_left->accept(*this);
    auto left_value = m_current_value;
    node->m_right->accept(*this);
    auto right_value = m_current_value;

    TypeCheckVisitor type_checker;
    node->m_left->accept(type_checker);
    Type expr_type = type_checker.get_deduced_type();
    if (expr_type == Type::INT32)
        m_current_value =
            builder.CreateICmpSGT(left_value, right_value, "gttmp");
    else if (expr_type == Type::FLOAT64)
        m_current_value =
            builder.CreateFCmpUGT(left_value, right_value, "gttmp");
}

void CodeGenVisitor::visit(LEqExprAST *node)
{
    node->m_left->accept(*this);
    auto left_value = m_current_value;
    node->m_right->accept(*this);
    auto right_value = m_current_value;

    TypeCheckVisitor type_checker;
    node->m_left->accept(type_checker);
    Type expr_type = type_checker.get_deduced_type();
    if (expr_type == Type::INT32)
        m_current_value =
            builder.CreateICmpSLE(left_value, right_value, "ltetmp");
    else if (expr_type == Type::FLOAT64)
        m_current_value =
            builder.CreateFCmpULE(left_value, right_value, "ltetmp");
}

void CodeGenVisitor::visit(GEqExprAST *node)
{
    node->m_left->accept(*this);
    auto left_value = m_current_value;
    node->m_right->accept(*this);
    auto right_value = m_current_value;

    TypeCheckVisitor type_checker;
    node->m_left->accept(type_checker);
    Type expr_type = type_checker.get_deduced_type();
    if (expr_type == Type::INT32)
        m_current_value =
            builder.CreateICmpSGE(left_value, right_value, "gtetmp");
    else if (expr_type == Type::FLOAT64)
        m_current_value =
            builder.CreateFCmpUGE(left_value, right_value, "gtetmp");
}

void CodeGenVisitor::visit(AndExprAST *node)
{
    node->m_left->accept(*this);
    auto left_value = m_current_value;
    node->m_right->accept(*this);
    auto right_value = m_current_value;
    m_current_value = builder.CreateAnd(left_value, right_value, "andtmp");
}

void CodeGenVisitor::visit(OrExprAST *node)
{
    node->m_left->accept(*this);
    auto left_value = m_current_value;
    node->m_right->accept(*this);
    auto right_value = m_current_value;
    m_current_value = builder.CreateOr(left_value, right_value, "ortmp");
}

void CodeGenVisitor::visit(NegExprAST *node)
{
    node->m_expr->accept(*this);
    TypeCheckVisitor type_checker;
    node->m_expr->accept(type_checker);
    Type expr_type = type_checker.get_deduced_type();
    if (expr_type == Type::INT32)
        m_current_value = builder.CreateNeg(m_current_value, "negtmp");
    else
        m_current_value = builder.CreateFNeg(m_current_value, "negtmp");
}

void CodeGenVisitor::visit(NotExprAST *node)
{
    node->m_expr->accept(*this);
    m_current_value = builder.CreateNot(m_current_value, "nottmp");
}

void CodeGenVisitor::visit(AssignExprAST *node)
{
    node->m_expr->accept(*this);
    builder.CreateStore(m_current_value, node->m_entry->address);
}

void CodeGenVisitor::visit(CallExprAST *node)
{
    llvm::Function *f = node->m_entry->callee;
    std::vector<llvm::Value *> realized_args;
    for (auto &arg : node->m_args) {
        arg->accept(*this);
        realized_args.push_back(m_current_value);
    }
    m_current_value = builder.CreateCall(f, realized_args, "calltmp");
}

void CodeGenVisitor::visit(ExpressionStmtAST *node)
{
    node->m_expr->accept(*this);
}

void CodeGenVisitor::visit(DeclarationStmtAST *node)
{
    llvm::IRBuilder<> tmp_builder(
        &(builder.GetInsertBlock()->getParent()->getEntryBlock()),
        builder.GetInsertBlock()->getParent()->getEntryBlock().begin());
    llvm::Type *llvm_type = core_to_llvm(node->m_entry->type, context);
    llvm::AllocaInst *addr =
        tmp_builder.CreateAlloca(llvm_type, nullptr, node->m_name.to_string());
    node->m_entry->address = addr;
}

void CodeGenVisitor::visit(PrintStmtAST *node)
{
    if (!m_int_print_fmt)
        m_int_print_fmt = builder.CreateGlobalStringPtr("%d\n");
    if (!m_float64_print_fmt)
        m_float64_print_fmt = builder.CreateGlobalStringPtr("%lf\n");

    node->m_expr->accept(*this);
    std::vector<llvm::Value *> print_args;

    TypeCheckVisitor type_checker;
    node->m_expr->accept(type_checker);
    Type expr_type = type_checker.get_deduced_type();

    if (expr_type == Type::INT32 || expr_type == Type::BOOL)
        print_args.push_back(m_int_print_fmt);
    else if (expr_type == Type::FLOAT64)
        print_args.push_back(m_float64_print_fmt);

    print_args.push_back(m_current_value);
    builder.CreateCall(m_print_function, print_args, "printfcall");
}

void CodeGenVisitor::visit(ReturnStmtAST *node)
{
    // This way of generating IR for return statements
    // creates lots of obsolete basic blocks. However,
    // these are easily optimized out.
    node->m_expr->accept(*this);
    builder.CreateRet(m_current_value);
    llvm::Function *f = builder.GetInsertBlock()->getParent();
    llvm::BasicBlock *after_ret_bb =
        llvm::BasicBlock::Create(context, "afterret", f);
    builder.SetInsertPoint(after_ret_bb);
}

void CodeGenVisitor::visit(CompoundStmtAST *node)
{
    for (auto &stmt : node->m_body)
        stmt->accept(*this);
}

void CodeGenVisitor::visit(IfThenStmtAST *node)
{
    llvm::Function *f = builder.GetInsertBlock()->getParent();
    llvm::BasicBlock *then_bb =
        llvm::BasicBlock::Create(context, "thenbody", f);
    llvm::BasicBlock *merge_bb = llvm::BasicBlock::Create(context, "merge", f);

    node->m_cond->accept(*this);
    builder.CreateCondBr(m_current_value, then_bb, merge_bb);

    builder.SetInsertPoint(then_bb);
    node->m_then_stmt->accept(*this);
    builder.CreateBr(merge_bb);

    builder.SetInsertPoint(merge_bb);
}

void CodeGenVisitor::visit(IfThenElseStmtAST *node)
{
    llvm::Function *f = builder.GetInsertBlock()->getParent();
    llvm::BasicBlock *then_bb =
        llvm::BasicBlock::Create(context, "thenbody", f);
    llvm::BasicBlock *else_bb =
        llvm::BasicBlock::Create(context, "elsebody", f);
    llvm::BasicBlock *merge_bb = llvm::BasicBlock::Create(context, "merge", f);

    node->m_cond->accept(*this);
    builder.CreateCondBr(m_current_value, then_bb, else_bb);

    builder.SetInsertPoint(then_bb);
    node->m_then_stmt->accept(*this);
    builder.CreateBr(merge_bb);

    builder.SetInsertPoint(else_bb);
    node->m_else_stmt->accept(*this);
    builder.CreateBr(merge_bb);

    builder.SetInsertPoint(merge_bb);
}

void CodeGenVisitor::visit(WhileStmtAST *node)
{
    llvm::Function *f = builder.GetInsertBlock()->getParent();
    llvm::BasicBlock *cond_bb = llvm::BasicBlock::Create(context, "cond", f);
    llvm::BasicBlock *loop_bb =
        llvm::BasicBlock::Create(context, "loopbody", f);
    llvm::BasicBlock *merge_bb = llvm::BasicBlock::Create(context, "merge", f);

    builder.CreateBr(cond_bb);
    builder.SetInsertPoint(cond_bb);

    node->m_cond->accept(*this);
    builder.CreateCondBr(m_current_value, loop_bb, merge_bb);

    builder.SetInsertPoint(loop_bb);
    node->m_body->accept(*this);
    builder.CreateBr(cond_bb);

    builder.SetInsertPoint(merge_bb);
}

void CodeGenVisitor::visit(ImportAST *node)
{
    auto [module, module_prefix] =
        m_module_manager.get_module(node->m_module_location);
    // Do not generate declarations multiple times in case
    // the same module has been imported more than once.
    if (m_imported_modules.find(module_prefix) != m_imported_modules.end())
        return;
    m_imported_modules.insert(module_prefix);
    for (auto &func : module->m_function_sequence) {
        auto accessed_func_name =
            func->m_name.to_string() == "main"
                ? CoreIdentifier("", func->m_name.to_string())
                : CoreIdentifier(module_prefix, func->m_name.to_string());
        std::vector<llvm::Type *> param_types;
        std::transform(
            func->m_entry->param_types.begin(),
            func->m_entry->param_types.end(), std::back_inserter(param_types),
            [this](const auto &param) { return core_to_llvm(param, context); });
        llvm::Type *return_type =
            core_to_llvm(func->m_entry->return_type, context);
        llvm::FunctionType *ft =
            llvm::FunctionType::get(return_type, param_types, false);
        llvm::Function *f = llvm::Function::Create(
            ft, llvm::Function::ExternalLinkage, accessed_func_name.to_string(),
            m_current_module.get());
        func->m_entry->callee = f;
    }
}

void CodeGenVisitor::visit(FunctionAST *node)
{
    llvm::Function *f = node->m_entry->callee;

    llvm::BasicBlock *entry_bb = llvm::BasicBlock::Create(context, "entry", f);
    builder.SetInsertPoint(entry_bb);

    size_t i = 0;
    for (auto &arg : f->args()) {
        arg.setName(node->m_params[i].second.to_string());
        llvm::IRBuilder<> tmp_builder(&(f->getEntryBlock()),
                                      f->getEntryBlock().begin());
        llvm::AllocaInst *addr = tmp_builder.CreateAlloca(
            f->getFunctionType()->getParamType(i), nullptr, arg.getName());
        builder.CreateStore(&arg, addr);
        node->m_param_entries[i++]->address = addr;
    }

    for (auto &stmt : node->m_body)
        stmt->accept(*this);

    Type return_type = node->m_entry->return_type;
    if (return_type == Type::INT32 || return_type == Type::BOOL)
        builder.CreateRet(llvm::ConstantInt::get(f->getReturnType(), 0));
    else if (return_type == Type::FLOAT64)
        builder.CreateRet(llvm::ConstantFP::get(f->getReturnType(), 0.0));

    llvm::verifyFunction(*f);
}

void CodeGenVisitor::visit(ModuleAST *node)
{
    auto source_name = node->m_location.get_source_name();
    m_current_module = std::make_unique<llvm::Module>(source_name, context);

    m_imported_modules.clear();
    for (auto &import : node->m_import_sequence)
        import->accept(*this);

    auto [_, module_prefix] = m_module_manager.get_module(source_name);
    for (auto &func : node->m_function_sequence) {
        auto accessed_func_name =
            func->m_name.to_string() == "main"
                ? CoreIdentifier("", func->m_name.to_string())
                : CoreIdentifier(module_prefix, func->m_name.to_string());
        std::vector<llvm::Type *> param_types;
        std::transform(
            func->m_entry->param_types.begin(),
            func->m_entry->param_types.end(), std::back_inserter(param_types),
            [this](const auto &param) { return core_to_llvm(param, context); });
        llvm::Type *return_type =
            core_to_llvm(func->m_entry->return_type, context);
        llvm::FunctionType *ft =
            llvm::FunctionType::get(return_type, param_types, false);
        llvm::Function *f = llvm::Function::Create(
            ft, llvm::Function::ExternalLinkage, accessed_func_name.to_string(),
            m_current_module.get());
        func->m_entry->callee = f;
    }

    llvm::FunctionType *print_ft = llvm::FunctionType::get(
        llvm::IntegerType::getInt32Ty(context),
        llvm::PointerType::get(llvm::Type::getInt8Ty(context), 0), true);
    m_print_function =
        llvm::Function::Create(print_ft, llvm::Function::ExternalLinkage,
                               "printf", m_current_module.get());

    for (auto &func : node->m_function_sequence)
        func->accept(*this);

    m_int_print_fmt = nullptr;
    m_float64_print_fmt = nullptr;
}

std::unique_ptr<llvm::Module> CodeGenVisitor::get_module()
{
    return std::move(m_current_module);
}

llvm::LLVMContext &CodeGenVisitor::get_context()
{
    return context;
}
