#ifndef CORE_DRIVER_HPP
#define CORE_DRIVER_HPP

#include "core_ast.hpp"
#include "core_parser.tab.hpp"

#include <iostream>

// Redefining yylex's signature (by default it is yylex(void), but
// it has to accept the driver as a parameter).
#define YY_DECL yy::parser::symbol_type yylex(CoreDriver &driver)

// Class for controlling the language lexer and parser.
class CoreDriver
{
    // The parser has to access the m_ast field to populate it,
    // so we'll set it as a friend class.
    friend class yy::parser;
    // The lexer has to access the m_loc field, so we're marking
    // the main lexing function (yylex) as friend.
    friend YY_DECL;

  public:
    CoreDriver(std::ostream &err_log = std::cerr);
    std::unique_ptr<ModuleAST>
    parse_module(std::istream &input = std::cin,
                 const std::string &source_name = "");
    std::unique_ptr<ModuleAST> parse_module(const std::string &source_name);

  private:
    std::unique_ptr<ModuleAST> m_ast;
    std::string m_source_name;
    yy::location m_loc;
    bool m_err_happened = false;
    std::ostream &m_err_log;
};

// Declaring yylex for the parser.
YY_DECL;

#endif // CORE_DRIVER_HPP
