#ifndef CORE_TYPECHECK_HPP
#define CORE_TYPECHECK_HPP

#include "core_ast.hpp"
#include "core_types.hpp"
#include "core_visitor.hpp"

#include <iostream>

class TypeCheckVisitor : public ASTVisitor
{
  public:
    TypeCheckVisitor(std::ostream &err_log = std::cerr);
    TypeCheckVisitor(NodeAST *node, std::ostream &err_log = std::cerr);

    void visit(I32ConstExprAST *node);
    void visit(F64ConstExprAST *node);
    void visit(BoolConstExprAST *node);
    void visit(VarExprAST *node);
    void visit(AddExprAST *node);
    void visit(SubExprAST *node);
    void visit(MulExprAST *node);
    void visit(DivExprAST *node);
    void visit(ModExprAST *node);
    void visit(EqExprAST *node);
    void visit(NEqExprAST *node);
    void visit(LTExprAST *node);
    void visit(GTExprAST *node);
    void visit(LEqExprAST *node);
    void visit(GEqExprAST *node);
    void visit(AndExprAST *node);
    void visit(OrExprAST *node);
    void visit(NegExprAST *node);
    void visit(NotExprAST *node);
    void visit(AssignExprAST *node);
    void visit(CallExprAST *node);

    Type get_deduced_type();
    operator Type()
    {
        return get_deduced_type();
    }

  private:
    Type m_deduced_type = Type::ERROR;
    std::ostream &m_err_log;
};

#endif // CORE_TYPECHECK_HPP
