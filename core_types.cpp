#include "core_types.hpp"

#include <unordered_map>

Type name_to_type(const std::string &name)
{
    static const std::unordered_map<std::string, Type> name_to_type_map = {
        {"int32", Type::INT32},
        {"float64", Type::FLOAT64},
        {"bool", Type::BOOL}};

    auto it = name_to_type_map.find(name);
    if (it == name_to_type_map.end())
        return Type::ERROR;
    return it->second;
}

bool is_numeric(const Type &type)
{
    return type == Type::INT32 || type == Type::FLOAT64;
}

llvm::Type *core_to_llvm(Type core_type, llvm::LLVMContext &context)
{
    switch (core_type) {
        case Type::INT32:
            return llvm::Type::getInt32Ty(context);
        case Type::FLOAT64:
            return llvm::Type::getDoubleTy(context);
        case Type::BOOL:
            return llvm::Type::getInt1Ty(context);
        default:
            return nullptr;
    }
}
