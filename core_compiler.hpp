#ifndef CORE_COMPILER_HPP
#define CORE_COMPILER_HPP

#include <iostream>
#include <string>

class ModuleManager;

class CoreCompiler
{
  public:
    CoreCompiler(std::ostream &err_log = std::cerr);
    bool compile(const std::string &input_path);

  private:
    bool resolve_names(ModuleManager &module_manager);
    bool check_wellformedness(ModuleManager &module_manager);
    void codegen(ModuleManager &module_manager);

    std::ostream &m_err_log;
};

#endif // CORE_COMPILER_HPP
