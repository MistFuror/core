#ifndef CORE_AST_HPP
#define CORE_AST_HPP

#include "core_identifier.hpp"
#include "core_location.hpp"
#include "core_symtable_entries.hpp"

#include <memory>
#include <unordered_map>
#include <vector>

class ASTVisitor;

class NodeAST
{
  public:
    NodeAST(const CoreLocation &loc);
    virtual ~NodeAST() = 0;
    virtual void accept(ASTVisitor &visitor) = 0;

  protected:
    CoreLocation m_location;
};

class ExprAST : public NodeAST
{
  public:
    ExprAST(const CoreLocation &loc);
    virtual ~ExprAST() = 0;
    using NodeAST::accept;
};

class I32ConstExprAST : public ExprAST
{
    friend class EchoVisitor;
    friend class NameResVisitor;
    friend class TypeCheckVisitor;
    friend class CodeGenVisitor;

  public:
    I32ConstExprAST(std::int32_t value, const CoreLocation &loc);
    void accept(ASTVisitor &visitor);

  private:
    std::int32_t m_value;
};

class F64ConstExprAST : public ExprAST
{
    friend class EchoVisitor;
    friend class NameResVisitor;
    friend class TypeCheckVisitor;
    friend class CodeGenVisitor;

  public:
    F64ConstExprAST(double value, const CoreLocation &loc);
    void accept(ASTVisitor &visitor);

  private:
    double m_value;
};

class BoolConstExprAST : public ExprAST
{
    friend class EchoVisitor;
    friend class NameResVisitor;
    friend class TypeCheckVisitor;
    friend class CodeGenVisitor;

  public:
    BoolConstExprAST(bool value, const CoreLocation &loc);
    void accept(ASTVisitor &visitor);

  private:
    bool m_value;
};

class VarExprAST : public ExprAST
{
    friend class EchoVisitor;
    friend class NameResVisitor;
    friend class TypeCheckVisitor;
    friend class CodeGenVisitor;

  public:
    VarExprAST(const CoreIdentifier &name, const CoreLocation &loc);
    void accept(ASTVisitor &visitor);

  private:
    CoreIdentifier m_name;
    std::shared_ptr<VariableEntry> m_entry = nullptr;
};

class AddExprAST : public ExprAST
{
    friend class EchoVisitor;
    friend class NameResVisitor;
    friend class TypeCheckVisitor;
    friend class CodeGenVisitor;

  public:
    AddExprAST(std::unique_ptr<ExprAST> left, std::unique_ptr<ExprAST> right,
               const CoreLocation &loc);
    void accept(ASTVisitor &visitor);

  private:
    std::unique_ptr<ExprAST> m_left, m_right;
};

class SubExprAST : public ExprAST
{
    friend class EchoVisitor;
    friend class NameResVisitor;
    friend class TypeCheckVisitor;
    friend class CodeGenVisitor;

  public:
    SubExprAST(std::unique_ptr<ExprAST> left, std::unique_ptr<ExprAST> right,
               const CoreLocation &loc);
    void accept(ASTVisitor &visitor);

  private:
    std::unique_ptr<ExprAST> m_left, m_right;
};

class MulExprAST : public ExprAST
{
    friend class EchoVisitor;
    friend class NameResVisitor;
    friend class TypeCheckVisitor;
    friend class CodeGenVisitor;

  public:
    MulExprAST(std::unique_ptr<ExprAST> left, std::unique_ptr<ExprAST> right,
               const CoreLocation &loc);
    void accept(ASTVisitor &visitor);

  private:
    std::unique_ptr<ExprAST> m_left, m_right;
};

class DivExprAST : public ExprAST
{
    friend class EchoVisitor;
    friend class NameResVisitor;
    friend class TypeCheckVisitor;
    friend class CodeGenVisitor;

  public:
    DivExprAST(std::unique_ptr<ExprAST> left, std::unique_ptr<ExprAST> right,
               const CoreLocation &loc);
    void accept(ASTVisitor &visitor);

  private:
    std::unique_ptr<ExprAST> m_left, m_right;
};

class ModExprAST : public ExprAST
{
    friend class EchoVisitor;
    friend class NameResVisitor;
    friend class TypeCheckVisitor;
    friend class CodeGenVisitor;

  public:
    ModExprAST(std::unique_ptr<ExprAST> left, std::unique_ptr<ExprAST> right,
               const CoreLocation &loc);
    void accept(ASTVisitor &visitor);

  private:
    std::unique_ptr<ExprAST> m_left, m_right;
};

class EqExprAST : public ExprAST
{
    friend class EchoVisitor;
    friend class NameResVisitor;
    friend class TypeCheckVisitor;
    friend class CodeGenVisitor;

  public:
    EqExprAST(std::unique_ptr<ExprAST> left, std::unique_ptr<ExprAST> right,
              const CoreLocation &loc);
    void accept(ASTVisitor &visitor);

  private:
    std::unique_ptr<ExprAST> m_left, m_right;
};

class NEqExprAST : public ExprAST
{
    friend class EchoVisitor;
    friend class NameResVisitor;
    friend class TypeCheckVisitor;
    friend class CodeGenVisitor;

  public:
    NEqExprAST(std::unique_ptr<ExprAST> left, std::unique_ptr<ExprAST> right,
               const CoreLocation &loc);
    void accept(ASTVisitor &visitor);

  private:
    std::unique_ptr<ExprAST> m_left, m_right;
};

class LTExprAST : public ExprAST
{
    friend class EchoVisitor;
    friend class NameResVisitor;
    friend class TypeCheckVisitor;
    friend class CodeGenVisitor;

  public:
    LTExprAST(std::unique_ptr<ExprAST> left, std::unique_ptr<ExprAST> right,
              const CoreLocation &loc);
    void accept(ASTVisitor &visitor);

  private:
    std::unique_ptr<ExprAST> m_left, m_right;
};

class GTExprAST : public ExprAST
{
    friend class EchoVisitor;
    friend class NameResVisitor;
    friend class TypeCheckVisitor;
    friend class CodeGenVisitor;

  public:
    GTExprAST(std::unique_ptr<ExprAST> left, std::unique_ptr<ExprAST> right,
              const CoreLocation &loc);
    void accept(ASTVisitor &visitor);

  private:
    std::unique_ptr<ExprAST> m_left, m_right;
};

class LEqExprAST : public ExprAST
{
    friend class EchoVisitor;
    friend class NameResVisitor;
    friend class TypeCheckVisitor;
    friend class CodeGenVisitor;

  public:
    LEqExprAST(std::unique_ptr<ExprAST> left, std::unique_ptr<ExprAST> right,
               const CoreLocation &loc);
    void accept(ASTVisitor &visitor);

  private:
    std::unique_ptr<ExprAST> m_left, m_right;
};

class GEqExprAST : public ExprAST
{
    friend class EchoVisitor;
    friend class NameResVisitor;
    friend class TypeCheckVisitor;
    friend class CodeGenVisitor;

  public:
    GEqExprAST(std::unique_ptr<ExprAST> left, std::unique_ptr<ExprAST> right,
               const CoreLocation &loc);
    void accept(ASTVisitor &visitor);

  private:
    std::unique_ptr<ExprAST> m_left, m_right;
};

class AndExprAST : public ExprAST
{
    friend class EchoVisitor;
    friend class NameResVisitor;
    friend class TypeCheckVisitor;
    friend class CodeGenVisitor;

  public:
    AndExprAST(std::unique_ptr<ExprAST> left, std::unique_ptr<ExprAST> right,
               const CoreLocation &loc);
    void accept(ASTVisitor &visitor);

  private:
    std::unique_ptr<ExprAST> m_left, m_right;
};

class OrExprAST : public ExprAST
{
    friend class EchoVisitor;
    friend class NameResVisitor;
    friend class TypeCheckVisitor;
    friend class CodeGenVisitor;

  public:
    OrExprAST(std::unique_ptr<ExprAST> left, std::unique_ptr<ExprAST> right,
              const CoreLocation &loc);
    void accept(ASTVisitor &visitor);

  private:
    std::unique_ptr<ExprAST> m_left, m_right;
};

class NegExprAST : public ExprAST
{
    friend class EchoVisitor;
    friend class NameResVisitor;
    friend class TypeCheckVisitor;
    friend class CodeGenVisitor;

  public:
    NegExprAST(std::unique_ptr<ExprAST> expr, const CoreLocation &loc);
    void accept(ASTVisitor &visitor);

  private:
    std::unique_ptr<ExprAST> m_expr;
};

class NotExprAST : public ExprAST
{
    friend class EchoVisitor;
    friend class NameResVisitor;
    friend class TypeCheckVisitor;
    friend class CodeGenVisitor;

  public:
    NotExprAST(std::unique_ptr<ExprAST> expr, const CoreLocation &loc);
    void accept(ASTVisitor &visitor);

  private:
    std::unique_ptr<ExprAST> m_expr;
};

class AssignExprAST : public ExprAST
{
    friend class EchoVisitor;
    friend class NameResVisitor;
    friend class TypeCheckVisitor;
    friend class CodeGenVisitor;

  public:
    AssignExprAST(const CoreIdentifier &name, std::unique_ptr<ExprAST> expr,
                  const CoreLocation &loc);
    void accept(ASTVisitor &visitor);

  private:
    CoreIdentifier m_name;
    std::unique_ptr<ExprAST> m_expr;
    std::shared_ptr<VariableEntry> m_entry = nullptr;
};

class CallExprAST : public ExprAST
{
    friend class EchoVisitor;
    friend class NameResVisitor;
    friend class TypeCheckVisitor;
    friend class CodeGenVisitor;

  public:
    CallExprAST(const CoreIdentifier &name,
                std::vector<std::unique_ptr<ExprAST>> &args,
                const CoreLocation &loc);
    void accept(ASTVisitor &visitor);

  private:
    CoreIdentifier m_name;
    std::vector<std::unique_ptr<ExprAST>> m_args;
    std::shared_ptr<FunctionEntry> m_entry = nullptr;
};

class StmtAST : public NodeAST
{
  public:
    StmtAST(const CoreLocation &loc);
    virtual ~StmtAST() = 0;
    using NodeAST::accept;

  protected:
    using NodeAST::m_location;
};

class ExpressionStmtAST : public StmtAST
{
    friend class EchoVisitor;
    friend class NameResVisitor;
    friend class WellFormedVisitor;
    friend class CodeGenVisitor;

  public:
    ExpressionStmtAST(std::unique_ptr<ExprAST> expr, const CoreLocation &loc);
    void accept(ASTVisitor &visitor);

  private:
    std::unique_ptr<ExprAST> m_expr;
};

class DeclarationStmtAST : public StmtAST
{
    friend class EchoVisitor;
    friend class NameResVisitor;
    friend class WellFormedVisitor;
    friend class CodeGenVisitor;

  public:
    DeclarationStmtAST(const std::string &type, const CoreIdentifier &name,
                       const CoreLocation &loc);
    void accept(ASTVisitor &visitor);

  private:
    std::string m_type;
    CoreIdentifier m_name;
    std::shared_ptr<VariableEntry> m_entry;
};

class PrintStmtAST : public StmtAST
{
    friend class EchoVisitor;
    friend class NameResVisitor;
    friend class WellFormedVisitor;
    friend class CodeGenVisitor;

  public:
    PrintStmtAST(std::unique_ptr<ExprAST> expr, const CoreLocation &loc);
    void accept(ASTVisitor &visitor);

  private:
    std::unique_ptr<ExprAST> m_expr;
};

class ReturnStmtAST : public StmtAST
{
    friend class EchoVisitor;
    friend class NameResVisitor;
    friend class WellFormedVisitor;
    friend class CodeGenVisitor;

  public:
    ReturnStmtAST(std::unique_ptr<ExprAST> expr, const CoreLocation &loc);
    void accept(ASTVisitor &visitor);

  private:
    std::unique_ptr<ExprAST> m_expr;
};

class CompoundStmtAST : public StmtAST
{
    friend class EchoVisitor;
    friend class NameResVisitor;
    friend class WellFormedVisitor;
    friend class CodeGenVisitor;

  public:
    CompoundStmtAST(std::vector<std::unique_ptr<StmtAST>> &body,
                    const CoreLocation &loc);
    void accept(ASTVisitor &visitor);

  private:
    std::vector<std::unique_ptr<StmtAST>> m_body;
};

class IfThenStmtAST : public StmtAST
{
    friend class EchoVisitor;
    friend class NameResVisitor;
    friend class WellFormedVisitor;
    friend class CodeGenVisitor;

  public:
    IfThenStmtAST(std::unique_ptr<ExprAST> cond,
                  std::unique_ptr<StmtAST> then_stmt, const CoreLocation &loc);
    void accept(ASTVisitor &visitor);

  private:
    std::unique_ptr<ExprAST> m_cond;
    std::unique_ptr<StmtAST> m_then_stmt;
};

class IfThenElseStmtAST : public StmtAST
{
    friend class EchoVisitor;
    friend class NameResVisitor;
    friend class WellFormedVisitor;
    friend class CodeGenVisitor;

  public:
    IfThenElseStmtAST(std::unique_ptr<ExprAST> cond,
                      std::unique_ptr<StmtAST> then_stmt,
                      std::unique_ptr<StmtAST> else_stmt,
                      const CoreLocation &loc);
    void accept(ASTVisitor &visitor);

  private:
    std::unique_ptr<ExprAST> m_cond;
    std::unique_ptr<StmtAST> m_then_stmt;
    std::unique_ptr<StmtAST> m_else_stmt;
};

class WhileStmtAST : public StmtAST
{
    friend class EchoVisitor;
    friend class NameResVisitor;
    friend class WellFormedVisitor;
    friend class CodeGenVisitor;

  public:
    WhileStmtAST(std::unique_ptr<ExprAST> cond, std::unique_ptr<StmtAST> body,
                 const CoreLocation &loc);
    void accept(ASTVisitor &visitor);

  private:
    std::unique_ptr<ExprAST> m_cond;
    std::unique_ptr<StmtAST> m_body;
};

class ImportAST : public NodeAST
{
    friend class ModuleManager;
    friend class EchoVisitor;
    friend class NameResVisitor;
    friend class WellFormedVisitor;
    friend class CodeGenVisitor;

  public:
    ImportAST(const std::string &module_location, const CoreIdentifier &name,
              const CoreLocation &loc);
    void accept(ASTVisitor &visitor);

  private:
    std::string m_module_location;
    CoreIdentifier m_name;
};

class FunctionAST : public NodeAST
{
    friend class EchoVisitor;
    friend class NameResVisitor;
    friend class WellFormedVisitor;
    friend class CodeGenVisitor;

  public:
    FunctionAST(
        const std::string &type, const CoreIdentifier &name,
        const std::vector<std::pair<std::string, CoreIdentifier>> &params,
        std::vector<std::unique_ptr<StmtAST>> &body, const CoreLocation &loc);
    void accept(ASTVisitor &visitor);

  private:
    std::string m_type;
    CoreIdentifier m_name;
    std::vector<std::pair<std::string, CoreIdentifier>> m_params;
    std::vector<std::unique_ptr<StmtAST>> m_body;
    std::shared_ptr<FunctionEntry> m_entry;
    std::vector<std::shared_ptr<VariableEntry>> m_param_entries;
};

class ModuleAST : public NodeAST
{
    friend class ModuleManager;
    friend class EchoVisitor;
    friend class NameResVisitor;
    friend class WellFormedVisitor;
    friend class CodeGenVisitor;

  public:
    ModuleAST(std::vector<std::unique_ptr<ImportAST>> &import_sequence,
              std::vector<std::unique_ptr<FunctionAST>> &function_sequence,
              const CoreLocation &loc);
    void accept(ASTVisitor &visitor);

  private:
    std::vector<std::unique_ptr<ImportAST>> m_import_sequence;
    std::vector<std::unique_ptr<FunctionAST>> m_function_sequence;
};

#endif // CORE_AST_HPP
