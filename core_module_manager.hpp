#ifndef CORE_MODULE_MANAGER_HPP
#define CORE_MODULE_MANAGER_HPP

#include "core_ast.hpp"

#include <iostream>
#include <map>
#include <unordered_map>
#include <utility>

class ModuleManager
{
    friend class CoreCompiler;

  public:
    ModuleManager(const std::string &main_module_path,
                  std::ostream &err_log = std::cerr);
    std::pair<ModuleAST *, std::string> get_module(const std::string &path);

  private:
    std::string m_main_module_path;
    std::unordered_map<std::string,
                       std::pair<std::unique_ptr<ModuleAST>, size_t>>
        m_modules;
    std::ostream &m_err_log;
};

#endif // CORE_MODULE_MANAGER_HPP
