#ifndef CORE_VISITOR_HPP
#define CORE_VISITOR_HPP

#include "core_ast.hpp"

// Interface class for all visitors which cover
// every type of AST node. In case a derived visitor
// doesn't define behaviour for a certain type of
// AST node, but a node of that type is accessed anyway,
// the program should be terminated due to an assert
// (default behaviour of this base class).
class ASTVisitor
{
  public:
    virtual ~ASTVisitor() = 0;

    virtual void visit(I32ConstExprAST *node);
    virtual void visit(F64ConstExprAST *node);
    virtual void visit(BoolConstExprAST *node);
    virtual void visit(VarExprAST *node);
    virtual void visit(AddExprAST *node);
    virtual void visit(SubExprAST *node);
    virtual void visit(MulExprAST *node);
    virtual void visit(DivExprAST *node);
    virtual void visit(ModExprAST *node);
    virtual void visit(EqExprAST *node);
    virtual void visit(NEqExprAST *node);
    virtual void visit(LTExprAST *node);
    virtual void visit(GTExprAST *node);
    virtual void visit(LEqExprAST *node);
    virtual void visit(GEqExprAST *node);
    virtual void visit(AndExprAST *node);
    virtual void visit(OrExprAST *node);
    virtual void visit(NegExprAST *node);
    virtual void visit(NotExprAST *node);
    virtual void visit(AssignExprAST *node);
    virtual void visit(CallExprAST *node);
    virtual void visit(ExpressionStmtAST *node);
    virtual void visit(DeclarationStmtAST *node);
    virtual void visit(PrintStmtAST *node);
    virtual void visit(ReturnStmtAST *node);
    virtual void visit(CompoundStmtAST *node);
    virtual void visit(IfThenStmtAST *node);
    virtual void visit(IfThenElseStmtAST *node);
    virtual void visit(WhileStmtAST *node);
    virtual void visit(ImportAST *node);
    virtual void visit(FunctionAST *node);
    virtual void visit(ModuleAST *node);
};

#endif // CORE_VISITOR_HPP
