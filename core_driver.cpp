#include "core_driver.hpp"

#include <filesystem>
#include <fstream>
#include <queue>

CoreDriver::CoreDriver(std::ostream &err_log)
    : m_err_log(err_log)
{
}

// The stream which the lexer gets input from.
extern std::istream *yyin_stream;

std::unique_ptr<ModuleAST>
CoreDriver::parse_module(std::istream &input, const std::string &source_name)
{
    m_err_happened = false;
    m_source_name = source_name;
    m_loc.initialize();
    yyin_stream = &input;
    yy::parser parser(*this);
    int res = parser();
    if (m_err_happened || res != 0)
        return nullptr;
    return std::move(m_ast);
}

std::unique_ptr<ModuleAST>
CoreDriver::parse_module(const std::string &source_name)
{
    std::ifstream input(source_name);
    if (!input.good()) {
        m_err_log << "Failed to open: " << source_name << std::endl;
        return nullptr;
    }
    return parse_module(input, source_name);
}
