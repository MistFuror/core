#include "core_ast.hpp"

#include "core_visitor.hpp"

#include <algorithm>

NodeAST::~NodeAST() {}
ExprAST::~ExprAST() {}
StmtAST::~StmtAST() {}

NodeAST::NodeAST(const CoreLocation &loc)
    : m_location(loc)
{
}

ExprAST::ExprAST(const CoreLocation &loc)
    : NodeAST(loc)
{
}

StmtAST::StmtAST(const CoreLocation &loc)
    : NodeAST(loc)
{
}

I32ConstExprAST::I32ConstExprAST(std::int32_t value, const CoreLocation &loc)
    : ExprAST(loc),
      m_value(value)
{
}

F64ConstExprAST::F64ConstExprAST(double value, const CoreLocation &loc)
    : ExprAST(loc),
      m_value(value)
{
}

BoolConstExprAST::BoolConstExprAST(bool value, const CoreLocation &loc)
    : ExprAST(loc),
      m_value(value)
{
}

VarExprAST::VarExprAST(const CoreIdentifier &name, const CoreLocation &loc)
    : ExprAST(loc),
      m_name(name)
{
}

AddExprAST::AddExprAST(std::unique_ptr<ExprAST> left,
                       std::unique_ptr<ExprAST> right, const CoreLocation &loc)
    : ExprAST(loc),
      m_left(std::move(left)),
      m_right(std::move(right))
{
}

SubExprAST::SubExprAST(std::unique_ptr<ExprAST> left,
                       std::unique_ptr<ExprAST> right, const CoreLocation &loc)
    : ExprAST(loc),
      m_left(std::move(left)),
      m_right(std::move(right))
{
}

MulExprAST::MulExprAST(std::unique_ptr<ExprAST> left,
                       std::unique_ptr<ExprAST> right, const CoreLocation &loc)
    : ExprAST(loc),
      m_left(std::move(left)),
      m_right(std::move(right))
{
}

DivExprAST::DivExprAST(std::unique_ptr<ExprAST> left,
                       std::unique_ptr<ExprAST> right, const CoreLocation &loc)
    : ExprAST(loc),
      m_left(std::move(left)),
      m_right(std::move(right))
{
}

ModExprAST::ModExprAST(std::unique_ptr<ExprAST> left,
                       std::unique_ptr<ExprAST> right, const CoreLocation &loc)
    : ExprAST(loc),
      m_left(std::move(left)),
      m_right(std::move(right))
{
}

EqExprAST::EqExprAST(std::unique_ptr<ExprAST> left,
                     std::unique_ptr<ExprAST> right, const CoreLocation &loc)
    : ExprAST(loc),
      m_left(std::move(left)),
      m_right(std::move(right))
{
}

NEqExprAST::NEqExprAST(std::unique_ptr<ExprAST> left,
                       std::unique_ptr<ExprAST> right, const CoreLocation &loc)
    : ExprAST(loc),
      m_left(std::move(left)),
      m_right(std::move(right))
{
}

LTExprAST::LTExprAST(std::unique_ptr<ExprAST> left,
                     std::unique_ptr<ExprAST> right, const CoreLocation &loc)
    : ExprAST(loc),
      m_left(std::move(left)),
      m_right(std::move(right))
{
}

GTExprAST::GTExprAST(std::unique_ptr<ExprAST> left,
                     std::unique_ptr<ExprAST> right, const CoreLocation &loc)
    : ExprAST(loc),
      m_left(std::move(left)),
      m_right(std::move(right))
{
}

LEqExprAST::LEqExprAST(std::unique_ptr<ExprAST> left,
                       std::unique_ptr<ExprAST> right, const CoreLocation &loc)
    : ExprAST(loc),
      m_left(std::move(left)),
      m_right(std::move(right))
{
}

GEqExprAST::GEqExprAST(std::unique_ptr<ExprAST> left,
                       std::unique_ptr<ExprAST> right, const CoreLocation &loc)
    : ExprAST(loc),
      m_left(std::move(left)),
      m_right(std::move(right))
{
}

AndExprAST::AndExprAST(std::unique_ptr<ExprAST> left,
                       std::unique_ptr<ExprAST> right, const CoreLocation &loc)
    : ExprAST(loc),
      m_left(std::move(left)),
      m_right(std::move(right))
{
}

OrExprAST::OrExprAST(std::unique_ptr<ExprAST> left,
                     std::unique_ptr<ExprAST> right, const CoreLocation &loc)
    : ExprAST(loc),
      m_left(std::move(left)),
      m_right(std::move(right))
{
}

NegExprAST::NegExprAST(std::unique_ptr<ExprAST> expr, const CoreLocation &loc)
    : ExprAST(loc),
      m_expr(std::move(expr))
{
}

NotExprAST::NotExprAST(std::unique_ptr<ExprAST> expr, const CoreLocation &loc)
    : ExprAST(loc),
      m_expr(std::move(expr))
{
}

AssignExprAST::AssignExprAST(const CoreIdentifier &name,
                             std::unique_ptr<ExprAST> expr,
                             const CoreLocation &loc)
    : ExprAST(loc),
      m_name(name),
      m_expr(std::move(expr))
{
}

CallExprAST::CallExprAST(const CoreIdentifier &name,
                         std::vector<std::unique_ptr<ExprAST>> &args,
                         const CoreLocation &loc)
    : ExprAST(loc),
      m_name(name),
      m_args(std::move(args))
{
}

ExpressionStmtAST::ExpressionStmtAST(std::unique_ptr<ExprAST> expr,
                                     const CoreLocation &loc)
    : StmtAST(loc),
      m_expr(std::move(expr))
{
}

DeclarationStmtAST::DeclarationStmtAST(const std::string &type,
                                       const CoreIdentifier &name,
                                       const CoreLocation &loc)
    : StmtAST(loc),
      m_type(type),
      m_name(name)
{
    m_entry =
        std::make_shared<VariableEntry>(VariableEntry{name_to_type(m_type)});
}

PrintStmtAST::PrintStmtAST(std::unique_ptr<ExprAST> expr,
                           const CoreLocation &loc)
    : StmtAST(loc),
      m_expr(std::move(expr))
{
}

ReturnStmtAST::ReturnStmtAST(std::unique_ptr<ExprAST> expr,
                             const CoreLocation &loc)
    : StmtAST(loc),
      m_expr(std::move(expr))
{
}

CompoundStmtAST::CompoundStmtAST(std::vector<std::unique_ptr<StmtAST>> &body,
                                 const CoreLocation &loc)
    : StmtAST(loc),
      m_body(std::move(body))
{
}

IfThenStmtAST::IfThenStmtAST(std::unique_ptr<ExprAST> cond,
                             std::unique_ptr<StmtAST> then_stmt,
                             const CoreLocation &loc)
    : StmtAST(loc),
      m_cond(std::move(cond)),
      m_then_stmt(std::move(then_stmt))
{
}

IfThenElseStmtAST::IfThenElseStmtAST(std::unique_ptr<ExprAST> cond,
                                     std::unique_ptr<StmtAST> then_stmt,
                                     std::unique_ptr<StmtAST> else_stmt,
                                     const CoreLocation &loc)
    : StmtAST(loc),
      m_cond(std::move(cond)),
      m_then_stmt(std::move(then_stmt)),
      m_else_stmt(std::move(else_stmt))
{
}

WhileStmtAST::WhileStmtAST(std::unique_ptr<ExprAST> cond,
                           std::unique_ptr<StmtAST> body,
                           const CoreLocation &loc)
    : StmtAST(loc),
      m_cond(std::move(cond)),
      m_body(std::move(body))
{
}

ImportAST::ImportAST(const std::string &module_location,
                     const CoreIdentifier &name, const CoreLocation &loc)
    : NodeAST(loc),
      m_module_location(module_location),
      m_name(name)
{
}

FunctionAST::FunctionAST(
    const std::string &type, const CoreIdentifier &name,
    const std::vector<std::pair<std::string, CoreIdentifier>> &params,
    std::vector<std::unique_ptr<StmtAST>> &body, const CoreLocation &loc)
    : NodeAST(loc),
      m_type(type),
      m_name(name),
      m_params(params),
      m_body(std::move(body))
{
    std::vector<Type> param_types;
    std::transform(m_params.begin(), m_params.end(),
                   std::back_inserter(param_types),
                   [](const auto &param) { return name_to_type(param.first); });
    m_entry = std::make_shared<FunctionEntry>(
        FunctionEntry{name_to_type(m_type), param_types});

    std::transform(
        param_types.begin(), param_types.end(),
        std::back_inserter(m_param_entries), [](const auto &param_type) {
            return std::make_shared<VariableEntry>(VariableEntry{param_type});
        });
}

ModuleAST::ModuleAST(
    std::vector<std::unique_ptr<ImportAST>> &import_sequence,
    std::vector<std::unique_ptr<FunctionAST>> &function_sequence,
    const CoreLocation &loc)
    : NodeAST(loc),
      m_import_sequence(std::move(import_sequence)),
      m_function_sequence(std::move(function_sequence))
{
}

void I32ConstExprAST::accept(ASTVisitor &visitor)
{
    visitor.visit(this);
}

void F64ConstExprAST::accept(ASTVisitor &visitor)
{
    visitor.visit(this);
}

void BoolConstExprAST::accept(ASTVisitor &visitor)
{
    visitor.visit(this);
}

void VarExprAST::accept(ASTVisitor &visitor)
{
    visitor.visit(this);
}

void AddExprAST::accept(ASTVisitor &visitor)
{
    visitor.visit(this);
}

void SubExprAST::accept(ASTVisitor &visitor)
{
    visitor.visit(this);
}

void MulExprAST::accept(ASTVisitor &visitor)
{
    visitor.visit(this);
}

void DivExprAST::accept(ASTVisitor &visitor)
{
    visitor.visit(this);
}

void ModExprAST::accept(ASTVisitor &visitor)
{
    visitor.visit(this);
}

void EqExprAST::accept(ASTVisitor &visitor)
{
    visitor.visit(this);
}

void NEqExprAST::accept(ASTVisitor &visitor)
{
    visitor.visit(this);
}

void LTExprAST::accept(ASTVisitor &visitor)
{
    visitor.visit(this);
}

void GTExprAST::accept(ASTVisitor &visitor)
{
    visitor.visit(this);
}

void LEqExprAST::accept(ASTVisitor &visitor)
{
    visitor.visit(this);
}

void GEqExprAST::accept(ASTVisitor &visitor)
{
    visitor.visit(this);
}

void AndExprAST::accept(ASTVisitor &visitor)
{
    visitor.visit(this);
}

void OrExprAST::accept(ASTVisitor &visitor)
{
    visitor.visit(this);
}

void NegExprAST::accept(ASTVisitor &visitor)
{
    visitor.visit(this);
}

void NotExprAST::accept(ASTVisitor &visitor)
{
    visitor.visit(this);
}

void AssignExprAST::accept(ASTVisitor &visitor)
{
    visitor.visit(this);
}

void CallExprAST::accept(ASTVisitor &visitor)
{
    visitor.visit(this);
}

void ExpressionStmtAST::accept(ASTVisitor &visitor)
{
    visitor.visit(this);
}

void DeclarationStmtAST::accept(ASTVisitor &visitor)
{
    visitor.visit(this);
}

void PrintStmtAST::accept(ASTVisitor &visitor)
{
    visitor.visit(this);
}

void ReturnStmtAST::accept(ASTVisitor &visitor)
{
    visitor.visit(this);
}

void CompoundStmtAST::accept(ASTVisitor &visitor)
{
    visitor.visit(this);
}

void IfThenStmtAST::accept(ASTVisitor &visitor)
{
    visitor.visit(this);
}

void IfThenElseStmtAST::accept(ASTVisitor &visitor)
{
    visitor.visit(this);
}

void WhileStmtAST::accept(ASTVisitor &visitor)
{
    visitor.visit(this);
}

void ImportAST::accept(ASTVisitor &visitor)
{
    visitor.visit(this);
}

void FunctionAST::accept(ASTVisitor &visitor)
{
    visitor.visit(this);
}

void ModuleAST::accept(ASTVisitor &visitor)
{
    visitor.visit(this);
}
