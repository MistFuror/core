#include "core_compiler.hpp"

#include "core_codegen.hpp"
#include "core_driver.hpp"
#include "core_module_manager.hpp"
#include "core_nameres.hpp"
#include "core_wellformed.hpp"

#include "llvm/Linker/Linker.h"

#include <optional>

CoreCompiler::CoreCompiler(std::ostream &err_log)
    : m_err_log(err_log)
{
}

bool CoreCompiler::compile(const std::string &input_path)
{
    std::optional<ModuleManager> module_manager;
    try {
        module_manager.emplace(input_path, m_err_log);
    } catch (...) {
        return false;
    }
    if (!resolve_names(*module_manager) ||
        !check_wellformedness(*module_manager))
        return false;
    codegen(*module_manager);
    return true;
}

bool CoreCompiler::resolve_names(ModuleManager &module_manager)
{
    NameResVisitor name_resolver(module_manager, m_err_log);
    bool is_valid = true;
    std::vector<std::string> modules_with_main;
    for (auto &[path, module] : module_manager.m_modules) {
        module.first->accept(name_resolver);
        if (!name_resolver.is_valid())
            is_valid = false;
        if (name_resolver.contains_main())
            modules_with_main.push_back(path);
    }
    if (modules_with_main.empty()) {
        m_err_log << "No main function found in modules" << std::endl;
        is_valid = false;
    } else if (modules_with_main.size() > 1) {
        m_err_log << "Multiple main functions defined in modules: ";
        for (auto &path : modules_with_main)
            m_err_log << "[" << path << "] ";
        m_err_log << std::endl;
        is_valid = false;
    }
    return is_valid;
}

bool CoreCompiler::check_wellformedness(ModuleManager &module_manager)
{
    WellFormedVisitor wellformedness_checker(module_manager, m_err_log);
    bool is_valid = true;
    for (auto &[_, module] : module_manager.m_modules) {
        module.first->accept(wellformedness_checker);
        if (!wellformedness_checker.is_valid())
            is_valid = false;
    }
    return is_valid;
}

void CoreCompiler::codegen(ModuleManager &module_manager)
{
    CodeGenVisitor code_generator(module_manager);
    llvm::Module product_llvm_module("product_module",
                                     code_generator.get_context());
    llvm::Linker linker(product_llvm_module);
    for (auto &[name, module] : module_manager.m_modules) {
        module.first->accept(code_generator);
        auto llvm_module = code_generator.get_module();
        linker.linkInModule(std::move(llvm_module));
    }
    product_llvm_module.print(llvm::outs(), nullptr);
}
