#ifndef CORE_IDENTIFIER_HPP
#define CORE_IDENTIFIER_HPP

#include <ostream>
#include <string>

// Class representing an identifier in a Core program,
// together with a possible module accessor.
class CoreIdentifier
{
  public:
    CoreIdentifier();
    CoreIdentifier(const std::string &module_identifier,
                   const std::string &real_identifier);
    std::string get_module_identifier();
    std::string get_real_identifier();
    std::string to_string();

  private:
    std::string m_module_identifier, m_real_identifier;

    friend std::ostream &operator<<(std::ostream &out,
                                    const CoreIdentifier &identifier);
};

#endif // CORE_IDENTIFIER_HPP
