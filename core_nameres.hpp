#ifndef CORE_NAMERES_HPP
#define CORE_NAMERES_HPP

#include "core_ast.hpp"
#include "core_module_manager.hpp"
#include "core_symtable.hpp"
#include "core_symtable_entries.hpp"
#include "core_visitor.hpp"

#include <iostream>
#include <memory>
#include <variant>

class NameResVisitor : public ASTVisitor
{
  public:
    NameResVisitor(ModuleManager &module_manager,
                   std::ostream &err_log = std::cerr);

    void visit(I32ConstExprAST *node);
    void visit(F64ConstExprAST *node);
    void visit(BoolConstExprAST *node);
    void visit(VarExprAST *node);
    void visit(AddExprAST *node);
    void visit(SubExprAST *node);
    void visit(MulExprAST *node);
    void visit(DivExprAST *node);
    void visit(ModExprAST *node);
    void visit(EqExprAST *node);
    void visit(NEqExprAST *node);
    void visit(LTExprAST *node);
    void visit(GTExprAST *node);
    void visit(LEqExprAST *node);
    void visit(GEqExprAST *node);
    void visit(AndExprAST *node);
    void visit(OrExprAST *node);
    void visit(NegExprAST *node);
    void visit(NotExprAST *node);
    void visit(AssignExprAST *node);
    void visit(CallExprAST *node);
    void visit(ExpressionStmtAST *node);
    void visit(DeclarationStmtAST *node);
    void visit(PrintStmtAST *node);
    void visit(ReturnStmtAST *node);
    void visit(CompoundStmtAST *node);
    void visit(IfThenStmtAST *node);
    void visit(IfThenElseStmtAST *node);
    void visit(WhileStmtAST *node);
    void visit(ImportAST *node);
    void visit(FunctionAST *node);
    void visit(ModuleAST *node);

    bool is_valid();
    bool contains_main();
    operator bool()
    {
        return is_valid();
    }

  private:
    bool m_success_flag = true;
    bool m_contains_main_flag = false;
    SymTable<std::variant<std::shared_ptr<VariableEntry>,
                          std::shared_ptr<FunctionEntry>, std::monostate>>
        m_symbol_table;
    ModuleManager &m_module_manager;
    std::ostream &m_err_log;
};

#endif // CORE_NAMERES_HPP
