#include "core_echo.hpp"

#include <iostream>

EchoVisitor::EchoVisitor() {}

EchoVisitor::EchoVisitor(NodeAST *node)
{
    node->accept(*this);
}

void EchoVisitor::visit(I32ConstExprAST *node)
{
    std::cout << node->m_value;
}

void EchoVisitor::visit(F64ConstExprAST *node)
{
    std::cout << node->m_value;
}

void EchoVisitor::visit(BoolConstExprAST *node)
{
    if (node->m_value)
        std::cout << "true";
    else
        std::cout << "false";
}

void EchoVisitor::visit(VarExprAST *node)
{
    std::cout << node->m_name;
}

void EchoVisitor::visit(AddExprAST *node)
{
    std::cout << "(";
    node->m_left->accept(*this);
    std::cout << " + ";
    node->m_right->accept(*this);
    std::cout << ")";
}

void EchoVisitor::visit(SubExprAST *node)
{
    std::cout << "(";
    node->m_left->accept(*this);
    std::cout << " - ";
    node->m_right->accept(*this);
    std::cout << ")";
}

void EchoVisitor::visit(MulExprAST *node)
{
    std::cout << "(";
    node->m_left->accept(*this);
    std::cout << " * ";
    node->m_right->accept(*this);
    std::cout << ")";
}

void EchoVisitor::visit(DivExprAST *node)
{
    std::cout << "(";
    node->m_left->accept(*this);
    std::cout << " / ";
    node->m_right->accept(*this);
    std::cout << ")";
}

void EchoVisitor::visit(ModExprAST *node)
{
    std::cout << "(";
    node->m_left->accept(*this);
    std::cout << " % ";
    node->m_right->accept(*this);
    std::cout << ")";
}

void EchoVisitor::visit(EqExprAST *node)
{
    std::cout << "(";
    node->m_left->accept(*this);
    std::cout << " == ";
    node->m_right->accept(*this);
    std::cout << ")";
}

void EchoVisitor::visit(NEqExprAST *node)
{
    std::cout << "(";
    node->m_left->accept(*this);
    std::cout << " != ";
    node->m_right->accept(*this);
    std::cout << ")";
}

void EchoVisitor::visit(LTExprAST *node)
{
    std::cout << "(";
    node->m_left->accept(*this);
    std::cout << " < ";
    node->m_right->accept(*this);
    std::cout << ")";
}

void EchoVisitor::visit(GTExprAST *node)
{
    std::cout << "(";
    node->m_left->accept(*this);
    std::cout << " > ";
    node->m_right->accept(*this);
    std::cout << ")";
}

void EchoVisitor::visit(LEqExprAST *node)
{
    std::cout << "(";
    node->m_left->accept(*this);
    std::cout << " <= ";
    node->m_right->accept(*this);
    std::cout << ")";
}

void EchoVisitor::visit(GEqExprAST *node)
{
    std::cout << "(";
    node->m_left->accept(*this);
    std::cout << " >= ";
    node->m_right->accept(*this);
    std::cout << ")";
}

void EchoVisitor::visit(AndExprAST *node)
{
    std::cout << "(";
    node->m_left->accept(*this);
    std::cout << " && ";
    node->m_right->accept(*this);
    std::cout << ")";
}

void EchoVisitor::visit(OrExprAST *node)
{
    std::cout << "(";
    node->m_left->accept(*this);
    std::cout << " || ";
    node->m_right->accept(*this);
    std::cout << ")";
}

void EchoVisitor::visit(NegExprAST *node)
{
    std::cout << "(";
    std::cout << "-";
    node->m_expr->accept(*this);
    std::cout << ")";
}

void EchoVisitor::visit(NotExprAST *node)
{
    std::cout << "(";
    std::cout << "!";
    node->m_expr->accept(*this);
    std::cout << ")";
}

void EchoVisitor::visit(AssignExprAST *node)
{
    std::cout << node->m_name << " = ";
    node->m_expr->accept(*this);
}

void EchoVisitor::visit(CallExprAST *node)
{
    std::cout << node->m_name << "(";
    auto sep = "";
    for (auto &arg : node->m_args) {
        std::cout << sep;
        sep = ", ";
        arg->accept(*this);
    }
    std::cout << ")";
}

void EchoVisitor::visit(ExpressionStmtAST *node)
{
    std::cout << std::string(m_indentation_level, '\t');
    node->m_expr->accept(*this);
    std::cout << ";\n";
}

void EchoVisitor::visit(DeclarationStmtAST *node)
{
    std::cout << std::string(m_indentation_level, '\t');
    std::cout << node->m_type << " " << node->m_name << ";\n";
}

void EchoVisitor::visit(PrintStmtAST *node)
{
    std::cout << std::string(m_indentation_level, '\t');
    std::cout << "print(";
    node->m_expr->accept(*this);
    std::cout << ");\n";
}

void EchoVisitor::visit(ReturnStmtAST *node)
{
    std::cout << std::string(m_indentation_level, '\t');
    std::cout << "return ";
    node->m_expr->accept(*this);
    std::cout << ";\n";
}

void EchoVisitor::visit(CompoundStmtAST *node)
{
    auto indent = std::string(m_indentation_level, '\t');
    std::cout << indent << "{\n";
    m_indentation_level++;
    for (auto &stmt : node->m_body)
        stmt->accept(*this);
    m_indentation_level--;
    std::cout << indent << "}\n";
}

void EchoVisitor::visit(IfThenStmtAST *node)
{
    auto indent = std::string(m_indentation_level, '\t');
    std::cout << indent << "if (";
    node->m_cond->accept(*this);
    std::cout << ")\n";
    std::cout << indent << "{\n";
    m_indentation_level++;
    node->m_then_stmt->accept(*this);
    m_indentation_level--;
    std::cout << indent << "}\n";
}

void EchoVisitor::visit(IfThenElseStmtAST *node)
{
    auto indent = std::string(m_indentation_level, '\t');
    std::cout << indent << "if (";
    node->m_cond->accept(*this);
    std::cout << ")\n";
    std::cout << indent << "{\n";
    m_indentation_level++;
    node->m_then_stmt->accept(*this);
    m_indentation_level--;
    std::cout << indent << "}\n";
    std::cout << indent << "else\n";
    std::cout << indent << "{\n";
    m_indentation_level++;
    node->m_else_stmt->accept(*this);
    m_indentation_level--;
    std::cout << indent << "}\n";
}

void EchoVisitor::visit(WhileStmtAST *node)
{
    auto indent = std::string(m_indentation_level, '\t');
    std::cout << indent << "while (";
    node->m_cond->accept(*this);
    std::cout << ")\n";
    std::cout << indent << "{\n";
    m_indentation_level++;
    node->m_body->accept(*this);
    m_indentation_level--;
    std::cout << indent << "}\n";
}

void EchoVisitor::visit(FunctionAST *node)
{
    std::cout << node->m_type << " " << node->m_name << "(";
    auto sep = "";
    for (auto &[type, name] : node->m_params) {
        std::cout << sep;
        sep = ", ";
        std::cout << type << " " << name;
    }
    std::cout << ")\n{\n";
    m_indentation_level++;
    for (auto &stmt : node->m_body)
        stmt->accept(*this);
    m_indentation_level--;
    std::cout << "}\n";
}

void EchoVisitor::visit(ModuleAST *node)
{
    for (auto &func : node->m_function_sequence) {
        func->accept(*this);
        std::cout << "\n";
    }
}
