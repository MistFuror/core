#include "core_module_manager.hpp"

#include "core_driver.hpp"
#include "core_visitor.hpp"

#include <exception>
#include <filesystem>
#include <queue>
#include <set>

ModuleManager::ModuleManager(const std::string &main_module_path,
                             std::ostream &err_log)
    : m_err_log(err_log)
{
    bool err_happened = false;
    std::queue<std::string> module_paths;
    std::set<std::string> visited_paths;

    try {
        m_main_module_path =
            std::filesystem::canonical(main_module_path).string();
        module_paths.push(m_main_module_path);
        visited_paths.insert(m_main_module_path);
    } catch (std::filesystem::filesystem_error &e) {
        m_err_log << "File does not exist: " << main_module_path << std::endl;
        err_happened = true;
    }

    CoreDriver driver;
    while (!module_paths.empty()) {
        auto module_path = module_paths.front();
        module_paths.pop();
        auto ast = driver.parse_module(module_path);
        if (!ast) {
            err_happened = true;
            continue;
        }
        m_modules[module_path] = {std::move(ast), m_modules.size()};
        for (auto &import : m_modules[module_path].first->m_import_sequence) {
            try {
                auto import_path =
                    std::filesystem::canonical(import->m_module_location)
                        .string();
                if (visited_paths.find(import_path) == visited_paths.end()) {
                    module_paths.push(import_path);
                    visited_paths.insert(import_path);
                }
            } catch (std::filesystem::filesystem_error &e) {
                m_err_log << "File does not exist: "
                          << import->m_module_location << std::endl;
                err_happened = true;
                continue;
            }
        }
    }

    if (err_happened)
        throw std::exception();
}

std::pair<ModuleAST *, std::string>
ModuleManager::get_module(const std::string &path)
{
    auto it = m_modules.find(std::filesystem::canonical(path).string());
    return {it->second.first.get(), "mod" + std::to_string(it->second.second)};
}
