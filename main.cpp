#include "core_compiler.hpp"

#include <iostream>

int main(int argc, char *argv[])
{
    if (argc != 2) {
        std::cerr << "Missing source name" << std::endl;
        return 0;
    }

    CoreCompiler compiler;
    compiler.compile(argv[1]);

    return 0;
}
