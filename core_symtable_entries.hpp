#ifndef CORE_SYMTABLE_ENTRIES_HPP
#define CORE_SYMTABLE_ENTRIES_HPP

#include "core_types.hpp"

#include "llvm/IR/Instructions.h"
#include "llvm/IR/Value.h"

#include <vector>

struct VariableEntry {
    Type type;
    llvm::AllocaInst *address = nullptr;
};

struct FunctionEntry {
    Type return_type;
    std::vector<Type> param_types;
    llvm::Function *callee = nullptr;
};

#endif // CORE_SYMTABLE_ENTRIES_HPP
