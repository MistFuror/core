#ifndef CORE_TYPES_HPP
#define CORE_TYPES_HPP

#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/Type.h"

#include <string>

// TODO: Types are currently represented with just an enum,
// this will probably change later on when class types are added.
enum class Type { INT32, FLOAT64, BOOL, ERROR };

Type name_to_type(const std::string &name);
bool is_numeric(const Type &type);
llvm::Type *core_to_llvm(Type core_type, llvm::LLVMContext &context);

#endif // CORE_TYPES_HPP
