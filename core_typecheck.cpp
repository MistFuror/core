#include "core_typecheck.hpp"

TypeCheckVisitor::TypeCheckVisitor(std::ostream &err_log)
    : m_err_log(err_log)
{
}

TypeCheckVisitor::TypeCheckVisitor(NodeAST *node, std::ostream &err_log)
    : m_err_log(err_log)
{
    node->accept(*this);
}

void TypeCheckVisitor::visit(I32ConstExprAST *node)
{
    m_deduced_type = Type::INT32;
}

void TypeCheckVisitor::visit(F64ConstExprAST *node)
{
    m_deduced_type = Type::FLOAT64;
}

void TypeCheckVisitor::visit(BoolConstExprAST *node)
{
    m_deduced_type = Type::BOOL;
}

void TypeCheckVisitor::visit(VarExprAST *node)
{
    if (!node->m_entry) {
        m_deduced_type = Type::ERROR;
        return;
    }
    m_deduced_type = node->m_entry->type;
}

void TypeCheckVisitor::visit(AddExprAST *node)
{
    node->m_left->accept(*this);
    Type left_type = m_deduced_type;
    node->m_right->accept(*this);
    Type right_type = m_deduced_type;
    if (left_type == Type::ERROR || right_type == Type::ERROR) {
        m_deduced_type = Type::ERROR;
        return;
    }
    if (left_type != right_type) {
        m_err_log << node->m_location << " error, type mismatch" << std::endl;
        m_deduced_type = Type::ERROR;
        return;
    }
    if (!is_numeric(left_type)) {
        m_err_log << node->m_location
                  << " error, expected numeric type in expression" << std::endl;
        m_deduced_type = Type::ERROR;
        return;
    }
    m_deduced_type = left_type;
}

void TypeCheckVisitor::visit(SubExprAST *node)
{
    node->m_left->accept(*this);
    Type left_type = m_deduced_type;
    node->m_right->accept(*this);
    Type right_type = m_deduced_type;
    if (left_type == Type::ERROR || right_type == Type::ERROR) {
        m_deduced_type = Type::ERROR;
        return;
    }
    if (left_type != right_type) {
        m_err_log << node->m_location << " error, type mismatch" << std::endl;
        m_deduced_type = Type::ERROR;
        return;
    }
    if (!is_numeric(left_type)) {
        m_err_log << node->m_location
                  << " error, expected numeric type in expression" << std::endl;
        m_deduced_type = Type::ERROR;
        return;
    }
    m_deduced_type = left_type;
}

void TypeCheckVisitor::visit(MulExprAST *node)
{
    node->m_left->accept(*this);
    Type left_type = m_deduced_type;
    node->m_right->accept(*this);
    Type right_type = m_deduced_type;
    if (left_type == Type::ERROR || right_type == Type::ERROR) {
        m_deduced_type = Type::ERROR;
        return;
    }
    if (left_type != right_type) {
        m_err_log << node->m_location << " error, type mismatch" << std::endl;
        m_deduced_type = Type::ERROR;
        return;
    }
    if (!is_numeric(left_type)) {
        m_err_log << node->m_location
                  << " error, expected numeric type in expression" << std::endl;
        m_deduced_type = Type::ERROR;
        return;
    }
    m_deduced_type = left_type;
}

void TypeCheckVisitor::visit(DivExprAST *node)
{
    node->m_left->accept(*this);
    Type left_type = m_deduced_type;
    node->m_right->accept(*this);
    Type right_type = m_deduced_type;
    if (left_type == Type::ERROR || right_type == Type::ERROR) {
        m_deduced_type = Type::ERROR;
        return;
    }
    if (left_type != right_type) {
        m_err_log << node->m_location << " error, type mismatch" << std::endl;
        m_deduced_type = Type::ERROR;
        return;
    }
    if (!is_numeric(left_type)) {
        m_err_log << node->m_location
                  << " error, expected numeric type in expression" << std::endl;
        m_deduced_type = Type::ERROR;
        return;
    }
    m_deduced_type = left_type;
}

void TypeCheckVisitor::visit(ModExprAST *node)
{
    node->m_left->accept(*this);
    Type left_type = m_deduced_type;
    node->m_right->accept(*this);
    Type right_type = m_deduced_type;
    if (left_type == Type::ERROR || right_type == Type::ERROR) {
        m_deduced_type = Type::ERROR;
        return;
    }
    if (left_type != right_type) {
        m_err_log << node->m_location << " error, type mismatch" << std::endl;
        m_deduced_type = Type::ERROR;
        return;
    }
    if (left_type != Type::INT32) {
        m_err_log << node->m_location
                  << " error, expected integer type in expression" << std::endl;
        m_deduced_type = Type::ERROR;
        return;
    }
    m_deduced_type = left_type;
}

void TypeCheckVisitor::visit(EqExprAST *node)
{
    node->m_left->accept(*this);
    Type left_type = m_deduced_type;
    node->m_right->accept(*this);
    Type right_type = m_deduced_type;
    if (left_type == Type::ERROR || right_type == Type::ERROR) {
        m_deduced_type = Type::ERROR;
        return;
    }
    if (left_type != right_type) {
        m_err_log << node->m_location << " error, type mismatch" << std::endl;
        m_deduced_type = Type::ERROR;
        return;
    }
    m_deduced_type = Type::BOOL;
}

void TypeCheckVisitor::visit(NEqExprAST *node)
{
    node->m_left->accept(*this);
    Type left_type = m_deduced_type;
    node->m_right->accept(*this);
    Type right_type = m_deduced_type;
    if (left_type == Type::ERROR || right_type == Type::ERROR) {
        m_deduced_type = Type::ERROR;
        return;
    }
    if (left_type != right_type) {
        m_err_log << node->m_location << " error, type mismatch" << std::endl;
        m_deduced_type = Type::ERROR;
        return;
    }
    m_deduced_type = Type::BOOL;
}

void TypeCheckVisitor::visit(LTExprAST *node)
{
    node->m_left->accept(*this);
    Type left_type = m_deduced_type;
    node->m_right->accept(*this);
    Type right_type = m_deduced_type;
    if (left_type == Type::ERROR || right_type == Type::ERROR) {
        m_deduced_type = Type::ERROR;
        return;
    }
    if (left_type != right_type) {
        m_err_log << node->m_location << " error, type mismatch" << std::endl;
        m_deduced_type = Type::ERROR;
        return;
    }
    if (!is_numeric(left_type)) {
        m_err_log << node->m_location
                  << " error, expected numeric type in expression" << std::endl;
        m_deduced_type = Type::ERROR;
        return;
    }
    m_deduced_type = Type::BOOL;
}

void TypeCheckVisitor::visit(GTExprAST *node)
{
    node->m_left->accept(*this);
    Type left_type = m_deduced_type;
    node->m_right->accept(*this);
    Type right_type = m_deduced_type;
    if (left_type == Type::ERROR || right_type == Type::ERROR) {
        m_deduced_type = Type::ERROR;
        return;
    }
    if (left_type != right_type) {
        m_err_log << node->m_location << " error, type mismatch" << std::endl;
        m_deduced_type = Type::ERROR;
        return;
    }
    if (!is_numeric(left_type)) {
        m_err_log << node->m_location
                  << " error, expected numeric type in expression" << std::endl;
        m_deduced_type = Type::ERROR;
        return;
    }
    m_deduced_type = Type::BOOL;
}

void TypeCheckVisitor::visit(LEqExprAST *node)
{
    node->m_left->accept(*this);
    Type left_type = m_deduced_type;
    node->m_right->accept(*this);
    Type right_type = m_deduced_type;
    if (left_type == Type::ERROR || right_type == Type::ERROR) {
        m_deduced_type = Type::ERROR;
        return;
    }
    if (left_type != right_type) {
        m_err_log << node->m_location << " error, type mismatch" << std::endl;
        m_deduced_type = Type::ERROR;
        return;
    }
    if (!is_numeric(left_type)) {
        m_err_log << node->m_location
                  << " error, expected numeric type in expression" << std::endl;
        m_deduced_type = Type::ERROR;
        return;
    }
    m_deduced_type = Type::BOOL;
}

void TypeCheckVisitor::visit(GEqExprAST *node)
{
    node->m_left->accept(*this);
    Type left_type = m_deduced_type;
    node->m_right->accept(*this);
    Type right_type = m_deduced_type;
    if (left_type == Type::ERROR || right_type == Type::ERROR) {
        m_deduced_type = Type::ERROR;
        return;
    }
    if (left_type != right_type) {
        m_err_log << node->m_location << " error, type mismatch" << std::endl;
        m_deduced_type = Type::ERROR;
        return;
    }
    if (!is_numeric(left_type)) {
        m_err_log << node->m_location
                  << " error, expected numeric type in expression" << std::endl;
        m_deduced_type = Type::ERROR;
        return;
    }
    m_deduced_type = Type::BOOL;
}

void TypeCheckVisitor::visit(AndExprAST *node)
{
    node->m_left->accept(*this);
    Type left_type = m_deduced_type;
    node->m_right->accept(*this);
    Type right_type = m_deduced_type;
    if (left_type == Type::ERROR || right_type == Type::ERROR) {
        m_deduced_type = Type::ERROR;
        return;
    }
    if (left_type != right_type) {
        m_err_log << node->m_location << " error, Type mismatch.\n";
        m_deduced_type = Type::ERROR;
        return;
    }
    if (left_type != Type::BOOL) {
        m_err_log << node->m_location
                  << " error, Expected bool type in expression.\n";
        m_deduced_type = Type::ERROR;
        return;
    }
    m_deduced_type = Type::BOOL;
}

void TypeCheckVisitor::visit(OrExprAST *node)
{
    node->m_left->accept(*this);
    Type left_type = m_deduced_type;
    node->m_right->accept(*this);
    Type right_type = m_deduced_type;
    if (left_type == Type::ERROR || right_type == Type::ERROR) {
        m_deduced_type = Type::ERROR;
        return;
    }
    if (left_type != right_type) {
        m_err_log << node->m_location << " error, type mismatch" << std::endl;
        m_deduced_type = Type::ERROR;
        return;
    }
    if (left_type != Type::BOOL) {
        m_err_log << node->m_location
                  << " error, expected bool type in expression" << std::endl;
        m_deduced_type = Type::ERROR;
        return;
    }
    m_deduced_type = Type::BOOL;
}

void TypeCheckVisitor::visit(NegExprAST *node)
{
    node->m_expr->accept(*this);
    // The expr_type variables aren't really needed.
    // There is lots of useless copying happening here,
    // but the code is, in my opinion, a bit more verbose.
    // Instead, we could directly operate on m_deduced_type.
    Type expr_type = m_deduced_type;
    if (expr_type == Type::ERROR) {
        m_deduced_type = Type::ERROR;
        return;
    }
    if (!is_numeric(expr_type)) {
        m_err_log << node->m_location
                  << " error, expected numeric type in expression" << std::endl;
        m_deduced_type = Type::ERROR;
        return;
    }
    m_deduced_type = expr_type;
}

void TypeCheckVisitor::visit(NotExprAST *node)
{
    node->m_expr->accept(*this);
    Type expr_type = m_deduced_type;
    if (expr_type == Type::ERROR) {
        m_deduced_type = Type::ERROR;
        return;
    }
    if (expr_type != Type::BOOL) {
        m_err_log << node->m_location
                  << " error, expected bool type in expression" << std::endl;
        m_deduced_type = Type::ERROR;
        return;
    }
    m_deduced_type = Type::BOOL;
}

void TypeCheckVisitor::visit(AssignExprAST *node)
{
    node->m_expr->accept(*this);
    Type expr_type = m_deduced_type;
    if (!node->m_entry) {
        m_deduced_type = Type::ERROR;
        return;
    }
    Type var_type = node->m_entry->type;
    if (var_type != expr_type) {
        m_err_log << node->m_location << " error, type mismatch" << std::endl;
        m_deduced_type = Type::ERROR;
        return;
    }
    m_deduced_type = var_type;
}

void TypeCheckVisitor::visit(CallExprAST *node)
{
    if (!node->m_entry) {
        m_deduced_type = Type::ERROR;
        return;
    }
    if (node->m_args.size() != node->m_entry->param_types.size()) {
        m_err_log << node->m_location
                  << " error, wrong number of arguments in function call"
                  << std::endl;
        m_deduced_type = Type::ERROR;
        return;
    }
    bool arg_type_mismatch = false;
    for (size_t i = 0; i < node->m_args.size(); ++i) {
        node->m_args[i]->accept(*this);
        Type arg_type = m_deduced_type;
        if (arg_type == Type::ERROR)
            arg_type_mismatch = true;
        else if (arg_type != node->m_entry->param_types[i]) {
            m_err_log << node->m_location
                      << " error, type mismatch of argument at position " << i
                      << std::endl;
            arg_type_mismatch = true;
        }
    }
    if (arg_type_mismatch) {
        m_deduced_type = Type::ERROR;
        return;
    }
    m_deduced_type = node->m_entry->return_type;
}

Type TypeCheckVisitor::get_deduced_type()
{
    return m_deduced_type;
}
