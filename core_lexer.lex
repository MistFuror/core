%option noyywrap
%option nounput
%option noinput
%option yylineno

%{

#include <cstdlib>
#include "core_driver.hpp"
#include "core_location.hpp"
#include "core_parser.tab.hpp"

extern CoreLocation bison_to_core_loc(const yy::parser::location_type &location, const std::string &source_name);

// Redefining YY_INPUT so that the lexer reads from an istream.
std::istream *yyin_stream = &std::cin;
#define YY_INPUT(buf, result, max_size) \
{ \
	if (yyin_stream->eof() || yyin_stream->fail()) \
		result = YY_NULL; \
	else { \
		yyin_stream->read(buf, max_size); \
		if (yyin_stream->bad()) \
			YY_FATAL_ERROR("YY_INPUT: Read from stream failed."); \
		else \
			result = yyin_stream->gcount(); \
	} \
}

#define YY_USER_ACTION driver.m_loc.columns(yyleng);

std::string string_literal_buffer;

%}

INT_LITERAL 0|[1-9][0-9]*
FLOAT_LITERAL [0-9]+\.[0-9]*
BOOL_LITERAL "true"|"false"

%x STRING_LITERAL

%%

%{
	driver.m_loc.step();
%}

\" {
	BEGIN STRING_LITERAL;
	string_literal_buffer = "";
}

<STRING_LITERAL>[^"]* {
	string_literal_buffer += yytext;
}

<STRING_LITERAL>\" {
	BEGIN INITIAL;
	return yy::parser::make_STRING_LITERAL(string_literal_buffer, driver.m_loc);
}

"int32" {
	return yy::parser::make_I32_TOKEN(driver.m_loc);
}

"float64" {
	return yy::parser::make_F64_TOKEN(driver.m_loc);
}

"bool" {
	return yy::parser::make_BOOL_TOKEN(driver.m_loc);
}

"print" {
	return yy::parser::make_PRINT_TOKEN(driver.m_loc);
}

"return" {
	return yy::parser::make_RETURN_TOKEN(driver.m_loc);
}

"if" {
	return yy::parser::make_IF_TOKEN(driver.m_loc);
}

"else" {
	return yy::parser::make_ELSE_TOKEN(driver.m_loc);
}

"while" {
	return yy::parser::make_WHILE_TOKEN(driver.m_loc);
}

"import" {
	return yy::parser::make_IMPORT_TOKEN(driver.m_loc);
}

"as" {
	return yy::parser::make_AS_TOKEN(driver.m_loc);
}

"::" {
	return yy::parser::make_MODULE_ACCESS_TOKEN(driver.m_loc);
}

"==" {
	return yy::parser::make_EQ_TOKEN(driver.m_loc);
}

"!=" {
	return yy::parser::make_NEQ_TOKEN(driver.m_loc);
}

"<=" {
	return yy::parser::make_LEQ_TOKEN(driver.m_loc);
}

">=" {
	return yy::parser::make_GEQ_TOKEN(driver.m_loc);
}

"&&" {
	return yy::parser::make_AND_TOKEN(driver.m_loc);
}

"||" {
	return yy::parser::make_OR_TOKEN(driver.m_loc);
}

"+" {
	return yy::parser::make_ADD_TOKEN(driver.m_loc);
}

"-" {
	return yy::parser::make_SUB_TOKEN(driver.m_loc);
}

"*" {
	return yy::parser::make_MUL_TOKEN(driver.m_loc);
}

"/" {
	return yy::parser::make_DIV_TOKEN(driver.m_loc);
}

"%" {
	return yy::parser::make_MOD_TOKEN(driver.m_loc);
}

"=" {
	return yy::parser::make_ASSIGN_TOKEN(driver.m_loc);
}

"(" {
	return yy::parser::make_LPARAN_TOKEN(driver.m_loc);
}

")" {
	return yy::parser::make_RPARAN_TOKEN(driver.m_loc);
}

"{" {
	return yy::parser::make_LBRACK_TOKEN(driver.m_loc);
}

"}" {
	return yy::parser::make_RBRACK_TOKEN(driver.m_loc);
}

";" {
	return yy::parser::make_SCOL_TOKEN(driver.m_loc);
}

"," {
	return yy::parser::make_COMMA_TOKEN(driver.m_loc);
}

"<" {
	return yy::parser::make_LT_TOKEN(driver.m_loc);
}

">" {
	return yy::parser::make_GT_TOKEN(driver.m_loc);
}

"!" {
	return yy::parser::make_NOT_TOKEN(driver.m_loc);
}

{INT_LITERAL} {
	return yy::parser::make_I32_LITERAL(atol(yytext), driver.m_loc);
}

{FLOAT_LITERAL} {
	return yy::parser::make_F64_LITERAL(atof(yytext), driver.m_loc);
}

{BOOL_LITERAL} {
	if (std::string(yytext) == "true")
		return yy::parser::make_BOOL_LITERAL(true, driver.m_loc);
	else
		return yy::parser::make_BOOL_LITERAL(false, driver.m_loc);
}

[a-zA-Z_][a-zA-Z0-9_]* {
	return yy::parser::make_ID_TOKEN(yytext, driver.m_loc);
}

[ \t\r]+ {
	driver.m_loc.step();
}

\n+ {
	driver.m_loc.lines(yyleng);
	driver.m_loc.step();
}

<<EOF>> {
	return yy::parser::make_END(driver.m_loc);
}

. {
	// TODO: Report from the parser instead - make a LEX_ERR_TOKEN to do that.
	driver.m_err_happened = true;
	driver.m_err_log << bison_to_core_loc(driver.m_loc, driver.m_source_name) << " " << "lexical error, cannot recognize symbol " << "'" << *yytext << "'" << std::endl;
	driver.m_loc.step();
}

%%
