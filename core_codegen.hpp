#ifndef CORE_CODEGEN_HPP
#define CORE_CODEGEN_HPP

#include "core_ast.hpp"
#include "core_module_manager.hpp"
#include "core_visitor.hpp"

#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/IR/Value.h"
#include "llvm/IR/Verifier.h"

#include <memory>
#include <set>

class CodeGenVisitor : public ASTVisitor
{
  public:
    CodeGenVisitor(ModuleManager &module_manager);

    void visit(I32ConstExprAST *node);
    void visit(F64ConstExprAST *node);
    void visit(BoolConstExprAST *node);
    void visit(VarExprAST *node);
    void visit(AddExprAST *node);
    void visit(SubExprAST *node);
    void visit(MulExprAST *node);
    void visit(DivExprAST *node);
    void visit(ModExprAST *node);
    void visit(EqExprAST *node);
    void visit(NEqExprAST *node);
    void visit(LTExprAST *node);
    void visit(GTExprAST *node);
    void visit(LEqExprAST *node);
    void visit(GEqExprAST *node);
    void visit(AndExprAST *node);
    void visit(OrExprAST *node);
    void visit(NegExprAST *node);
    void visit(NotExprAST *node);
    void visit(AssignExprAST *node);
    void visit(CallExprAST *node);
    void visit(ExpressionStmtAST *node);
    void visit(DeclarationStmtAST *node);
    void visit(PrintStmtAST *node);
    void visit(ReturnStmtAST *node);
    void visit(CompoundStmtAST *node);
    void visit(IfThenStmtAST *node);
    void visit(IfThenElseStmtAST *node);
    void visit(WhileStmtAST *node);
    void visit(ImportAST *node);
    void visit(FunctionAST *node);
    void visit(ModuleAST *node);

    std::unique_ptr<llvm::Module> get_module();
    llvm::LLVMContext &get_context();

  private:
    llvm::LLVMContext context;
    llvm::IRBuilder<> builder;
    llvm::Value *m_current_value = nullptr;
    std::unique_ptr<llvm::Module> m_current_module = nullptr;
    llvm::Value *m_int_print_fmt = nullptr;
    llvm::Value *m_float64_print_fmt = nullptr;
    llvm::Function *m_print_function = nullptr;
    std::set<std::string> m_imported_modules;
    ModuleManager &m_module_manager;
};

#endif // CORE_CODEGEN_HPP
