#ifndef CORE_LOCATION_HPP
#define CORE_LOCATION_HPP

#include <ostream>
#include <unordered_set>
#include <utility>

// Class representing a location in some Core source file.
class CoreLocation
{
  public:
    CoreLocation(const std::pair<size_t, size_t> &begin,
                 const std::pair<size_t, size_t> &end,
                 const std::string &source_name = "");
    std::string get_source_name();

  private:
    std::pair<size_t, size_t> m_begin, m_end;
    const std::string *m_source_name;

    // Static field for storing all occuring source names
    // in order to reduce memory usage (by CoreLocation storing
    // only a pointer to a source name in the static field
    // instead of the entire string.
    inline static std::unordered_set<std::string> source_names;

    friend std::ostream &operator<<(std::ostream &out, const CoreLocation &loc);
};

std::ostream &operator<<(std::ostream &out, const CoreLocation &loc);

#endif // CORE_LOCATION_HPP
