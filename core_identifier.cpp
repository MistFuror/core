#include "core_identifier.hpp"

#include <sstream>

CoreIdentifier::CoreIdentifier()
    : m_module_identifier(""),
      m_real_identifier("")
{
}

CoreIdentifier::CoreIdentifier(const std::string &module_identifier,
                               const std::string &real_identifier)
    : m_module_identifier(module_identifier),
      m_real_identifier(real_identifier)
{
}

std::string CoreIdentifier::get_module_identifier()
{
    return m_module_identifier;
}

std::string CoreIdentifier::get_real_identifier()
{
    return m_real_identifier;
}

std::string CoreIdentifier::to_string()
{
    std::stringstream ss;
    ss << *this;
    return ss.str();
}

std::ostream &operator<<(std::ostream &out, const CoreIdentifier &identifier)
{
    out << identifier.m_module_identifier
        << (identifier.m_module_identifier == "" ? "" : "::")
        << identifier.m_real_identifier;
    return out;
}
