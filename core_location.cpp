#include "core_location.hpp"

CoreLocation::CoreLocation(const std::pair<size_t, size_t> &begin,
                           const std::pair<size_t, size_t> &end,
                           const std::string &source_name)
    : m_begin(begin),
      m_end(end)
{
    auto [it, _] = source_names.insert(source_name);
    m_source_name = &(*it);
}

std::string CoreLocation::get_source_name()
{
    return *m_source_name;
}

std::ostream &operator<<(std::ostream &out, const CoreLocation &loc)
{
    out << "[" << *(loc.m_source_name) << ": " << loc.m_begin.first << ","
        << loc.m_begin.second << " - " << loc.m_end.first << ","
        << loc.m_end.second << "]";
    return out;
}
