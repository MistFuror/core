#ifndef CORE_WELLFORMED_HPP
#define CORE_WELLFORMED_HPP

#include "core_ast.hpp"
#include "core_module_manager.hpp"
#include "core_types.hpp"
#include "core_visitor.hpp"

#include <iostream>

class WellFormedVisitor : public ASTVisitor
{
  public:
    WellFormedVisitor(ModuleManager &module_manager,
                      std::ostream &err_log = std::cerr);

    void visit(ExpressionStmtAST *node);
    void visit(DeclarationStmtAST *node);
    void visit(PrintStmtAST *node);
    void visit(ReturnStmtAST *node);
    void visit(CompoundStmtAST *node);
    void visit(IfThenStmtAST *node);
    void visit(IfThenElseStmtAST *node);
    void visit(WhileStmtAST *node);
    void visit(FunctionAST *node);
    void visit(ModuleAST *node);

    bool is_valid();
    operator bool()
    {
        return is_valid();
    }

  private:
    bool m_well_formed_flag = true;
    Type m_current_function_type;
    ModuleManager &m_module_manager;
    std::ostream &m_err_log;
};

#endif // CORE_WELLFORMED_HPP
