#include "core_nameres.hpp"

#include <algorithm>

NameResVisitor::NameResVisitor(ModuleManager &module_manager,
                               std::ostream &err_log)
    : m_module_manager(module_manager),
      m_err_log(err_log)
{
}

void NameResVisitor::visit(I32ConstExprAST *node) {}

void NameResVisitor::visit(F64ConstExprAST *node) {}

void NameResVisitor::visit(BoolConstExprAST *node) {}

void NameResVisitor::visit(VarExprAST *node)
{
    auto symtable_entry =
        m_symbol_table.lookup_symbol(node->m_name.to_string());
    if (!symtable_entry) {
        m_err_log << node->m_location << " error, unknown symbol "
                  << node->m_name << std::endl;
        m_success_flag = false;
    } else
        node->m_entry =
            std::get<std::shared_ptr<VariableEntry>>(*symtable_entry);
}

void NameResVisitor::visit(AddExprAST *node)
{
    node->m_left->accept(*this);
    node->m_right->accept(*this);
}

void NameResVisitor::visit(SubExprAST *node)
{
    node->m_left->accept(*this);
    node->m_right->accept(*this);
}

void NameResVisitor::visit(MulExprAST *node)
{
    node->m_left->accept(*this);
    node->m_right->accept(*this);
}

void NameResVisitor::visit(DivExprAST *node)
{
    node->m_left->accept(*this);
    node->m_right->accept(*this);
}

void NameResVisitor::visit(ModExprAST *node)
{
    node->m_left->accept(*this);
    node->m_right->accept(*this);
}

void NameResVisitor::visit(EqExprAST *node)
{
    node->m_left->accept(*this);
    node->m_right->accept(*this);
}

void NameResVisitor::visit(NEqExprAST *node)
{
    node->m_left->accept(*this);
    node->m_right->accept(*this);
}

void NameResVisitor::visit(LTExprAST *node)
{
    node->m_left->accept(*this);
    node->m_right->accept(*this);
}

void NameResVisitor::visit(GTExprAST *node)
{
    node->m_left->accept(*this);
    node->m_right->accept(*this);
}

void NameResVisitor::visit(LEqExprAST *node)
{
    node->m_left->accept(*this);
    node->m_right->accept(*this);
}

void NameResVisitor::visit(GEqExprAST *node)
{
    node->m_left->accept(*this);
    node->m_right->accept(*this);
}

void NameResVisitor::visit(AndExprAST *node)
{
    node->m_left->accept(*this);
    node->m_right->accept(*this);
}

void NameResVisitor::visit(OrExprAST *node)
{
    node->m_left->accept(*this);
    node->m_right->accept(*this);
}

void NameResVisitor::visit(NegExprAST *node)
{
    node->m_expr->accept(*this);
}

void NameResVisitor::visit(NotExprAST *node)
{
    node->m_expr->accept(*this);
}

void NameResVisitor::visit(AssignExprAST *node)
{
    auto symtable_entry =
        m_symbol_table.lookup_symbol(node->m_name.to_string());
    if (!symtable_entry) {
        m_err_log << node->m_location << " error, unknown symbol "
                  << node->m_name << std::endl;
        m_success_flag = false;
    } else
        node->m_entry =
            std::get<std::shared_ptr<VariableEntry>>(*symtable_entry);
    node->m_expr->accept(*this);
}

void NameResVisitor::visit(CallExprAST *node)
{
    auto symtable_entry =
        m_symbol_table.lookup_symbol(node->m_name.to_string());
    if (!symtable_entry) {
        m_err_log << node->m_location << " error, unknown symbol "
                  << node->m_name << std::endl;
        m_success_flag = false;
    } else
        node->m_entry =
            std::get<std::shared_ptr<FunctionEntry>>(*symtable_entry);
    for (auto &arg : node->m_args)
        arg->accept(*this);
}

void NameResVisitor::visit(ExpressionStmtAST *node)
{
    node->m_expr->accept(*this);
}

void NameResVisitor::visit(DeclarationStmtAST *node)
{
    if (!m_symbol_table.insert_symbol(node->m_name.to_string(),
                                      node->m_entry)) {
        m_err_log << node->m_location << " error, symbol " << node->m_name
                  << " already declared in current scope" << std::endl;
        m_success_flag = false;
    }
}

void NameResVisitor::visit(PrintStmtAST *node)
{
    node->m_expr->accept(*this);
}

void NameResVisitor::visit(ReturnStmtAST *node)
{
    node->m_expr->accept(*this);
}

void NameResVisitor::visit(CompoundStmtAST *node)
{
    m_symbol_table.push_scope();
    for (auto &stmt : node->m_body)
        stmt->accept(*this);
    m_symbol_table.pop_scope();
}

void NameResVisitor::visit(IfThenStmtAST *node)
{
    node->m_cond->accept(*this);
    m_symbol_table.push_scope();
    node->m_then_stmt->accept(*this);
    m_symbol_table.pop_scope();
}

void NameResVisitor::visit(IfThenElseStmtAST *node)
{
    node->m_cond->accept(*this);
    m_symbol_table.push_scope();
    node->m_then_stmt->accept(*this);
    m_symbol_table.pop_scope();
    m_symbol_table.push_scope();
    node->m_else_stmt->accept(*this);
    m_symbol_table.pop_scope();
}

void NameResVisitor::visit(WhileStmtAST *node)
{
    node->m_cond->accept(*this);
    m_symbol_table.push_scope();
    node->m_body->accept(*this);
    m_symbol_table.pop_scope();
}

void NameResVisitor::visit(ImportAST *node)
{
    if (!m_symbol_table.insert_symbol(node->m_name.to_string(),
                                      std::monostate())) {
        m_err_log << node->m_location << " error, symbol " << node->m_name
                  << " already declared in current scope" << std::endl;
        m_success_flag = false;
    }
    auto [module, _] = m_module_manager.get_module(node->m_module_location);
    for (auto &func : module->m_function_sequence) {
        auto accessed_func_name =
            CoreIdentifier(node->m_name.to_string(), func->m_name.to_string());
        if (!m_symbol_table.insert_symbol(accessed_func_name.to_string(),
                                          func->m_entry)) {
            m_err_log << node->m_location << " error, symbol "
                      << accessed_func_name << " imported from "
                      << node->m_module_location
                      << " already declared in current scope" << std::endl;
            m_success_flag = false;
        }
    }
}

void NameResVisitor::visit(FunctionAST *node)
{
    m_symbol_table.push_scope();
    for (size_t i = 0; i < node->m_params.size(); ++i) {
        auto param_name = node->m_params[i].second;
        if (!m_symbol_table.insert_symbol(param_name.to_string(),
                                          node->m_param_entries[i])) {
            m_err_log << node->m_location << " error, symbol " << param_name
                      << " already declared in current scope" << std::endl;
            m_success_flag = false;
        }
    }
    for (auto &stmt : node->m_body)
        stmt->accept(*this);
    m_symbol_table.pop_scope();
}

void NameResVisitor::visit(ModuleAST *node)
{
    m_success_flag = true;
    m_contains_main_flag = false;

    m_symbol_table.push_scope();
    // We have to populate the global scope with
    // all imported function names first.
    for (auto &import : node->m_import_sequence)
        import->accept(*this);
    // We want to avoid having the declare functions
    // before calling them, so we're first populating
    // the global scope with their names before further
    // visiting the function bodies.
    for (auto &func : node->m_function_sequence) {
        if (func->m_name.to_string() == "main")
            m_contains_main_flag = true;
        if (!m_symbol_table.insert_symbol(func->m_name.to_string(),
                                          func->m_entry)) {
            m_err_log << node->m_location << " error, symbol " << func->m_name
                      << " already declared in current scope" << std::endl;
            m_success_flag = false;
        }
    }
    for (auto &func : node->m_function_sequence)
        func->accept(*this);
    m_symbol_table.pop_scope();
}

bool NameResVisitor::is_valid()
{
    return m_success_flag;
}

bool NameResVisitor::contains_main()
{
    return m_contains_main_flag;
}
