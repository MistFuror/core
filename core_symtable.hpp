#ifndef CORE_SYMTABLE_HPP
#define CORE_SYMTABLE_HPP

#include <memory>
#include <optional>
#include <unordered_map>

// Spaghetti stack implementation of a symbol table.
template <class T> class SymTable
{
    struct Scope {
        std::shared_ptr<Scope> parent;
        std::unordered_map<std::string, T> table;
    };

  public:
    void push_scope();
    void pop_scope();
    bool insert_symbol(const std::string &symbol, const T &entry);
    std::optional<T> lookup_symbol(const std::string &symbol);

  private:
    std::shared_ptr<Scope> m_current_scope = nullptr;
};

template <class T> void SymTable<T>::push_scope()
{
    auto new_scope = std::make_shared<Scope>();
    new_scope->parent = m_current_scope;
    m_current_scope = new_scope;
}

template <class T> void SymTable<T>::pop_scope()
{
    m_current_scope = m_current_scope->parent;
}

// Fails (returns false) if symbol already exists in current scope.
template <class T>
bool SymTable<T>::insert_symbol(const std::string &symbol, const T &entry)
{
    if (m_current_scope->table.find(symbol) != m_current_scope->table.end())
        return false;
    m_current_scope->table[symbol] = entry;
    return true;
}

template <class T>
std::optional<T> SymTable<T>::lookup_symbol(const std::string &symbol)
{
    auto lookup_scope = m_current_scope;
    while (lookup_scope != nullptr) {
        auto it = lookup_scope->table.find(symbol);
        if (it != lookup_scope->table.end())
            return it->second;
        lookup_scope = lookup_scope->parent;
    }
    return std::nullopt;
}

#endif // CORE_SYMTABLE_HPP
