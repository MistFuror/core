#include "core_wellformed.hpp"

#include "core_typecheck.hpp"

WellFormedVisitor::WellFormedVisitor(ModuleManager &module_manager,
                                     std::ostream &err_log)
    : m_module_manager(module_manager),
      m_err_log(err_log)
{
}

void WellFormedVisitor::visit(ExpressionStmtAST *node)
{
    TypeCheckVisitor type_checker;
    node->m_expr->accept(type_checker);
    if (type_checker.get_deduced_type() == Type::ERROR)
        m_well_formed_flag = false;
}

void WellFormedVisitor::visit(DeclarationStmtAST *node) {}

void WellFormedVisitor::visit(PrintStmtAST *node)
{
    TypeCheckVisitor type_checker;
    node->m_expr->accept(type_checker);
    if (type_checker.get_deduced_type() == Type::ERROR)
        m_well_formed_flag = false;
}

void WellFormedVisitor::visit(ReturnStmtAST *node)
{
    TypeCheckVisitor type_checker;
    node->m_expr->accept(type_checker);
    Type expr_type = type_checker.get_deduced_type();
    if (expr_type == Type::ERROR)
        m_well_formed_flag = false;
    else if (expr_type != m_current_function_type) {
        m_well_formed_flag = false;
        m_err_log << node->m_location
                  << " error, expression type of return statement does not "
                     "match with the return type of function"
                  << std::endl;
    }
}

void WellFormedVisitor::visit(CompoundStmtAST *node)
{
    for (auto &stmt : node->m_body)
        stmt->accept(*this);
}

void WellFormedVisitor::visit(IfThenStmtAST *node)
{
    TypeCheckVisitor type_checker;
    node->m_cond->accept(type_checker);
    Type cond_type = type_checker.get_deduced_type();
    if (cond_type == Type::ERROR)
        m_well_formed_flag = false;
    else if (cond_type != Type::BOOL) {
        m_well_formed_flag = false;
        m_err_log << node->m_location
                  << " error, expected expression of bool type as condition"
                  << std::endl;
    }
    node->m_then_stmt->accept(*this);
}

void WellFormedVisitor::visit(IfThenElseStmtAST *node)
{
    TypeCheckVisitor type_checker;
    node->m_cond->accept(type_checker);
    Type cond_type = type_checker.get_deduced_type();
    if (cond_type == Type::ERROR)
        m_well_formed_flag = false;
    else if (cond_type != Type::BOOL) {
        m_well_formed_flag = false;
        m_err_log << node->m_location
                  << " error, expected expression of bool type as condition"
                  << std::endl;
    }
    node->m_then_stmt->accept(*this);
    node->m_else_stmt->accept(*this);
}

void WellFormedVisitor::visit(WhileStmtAST *node)
{
    TypeCheckVisitor type_checker;
    node->m_cond->accept(type_checker);
    Type cond_type = type_checker.get_deduced_type();
    if (cond_type == Type::ERROR)
        m_well_formed_flag = false;
    else if (cond_type != Type::BOOL) {
        m_well_formed_flag = false;
        m_err_log << node->m_location
                  << " error, expected expression of bool type as condition"
                  << std::endl;
    }
    node->m_body->accept(*this);
}

void WellFormedVisitor::visit(FunctionAST *node)
{
    m_current_function_type = name_to_type(node->m_type);
    for (auto &stmt : node->m_body)
        stmt->accept(*this);
}

void WellFormedVisitor::visit(ModuleAST *node)
{
    m_well_formed_flag = true;
    for (auto &func : node->m_function_sequence)
        func->accept(*this);
}

bool WellFormedVisitor::is_valid()
{
    return m_well_formed_flag;
}
